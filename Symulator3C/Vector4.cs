﻿using System;
using System.Windows.Media.Media3D;

namespace Symulator3C
{
    public class Vector4
    {
        #region Properties
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }
        public double Fourth { get; set; }
        #endregion

        #region .ctors
        public Vector4(double x, double y, double z, double fourth)
        {
            X = x;
            Y = y;
            Z = z;
            Fourth = fourth;
        }

        public Vector4(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
            Fourth = 1.0;
        }

        #endregion
        public void Normalize()
        {
            if (Fourth == 0)
                return;

            X /= Fourth;
            Y /= Fourth;
            Z /= Fourth;
            Fourth /= Fourth;
        }
        public void NormalizedLenght()
        {
            double lenght = X * X + Y * Y + Z * Z + Fourth * Fourth;
            lenght = Math.Sqrt(lenght);
            X /= lenght;
            Y /= lenght;
            Z /= lenght;
            Fourth /= lenght;
        }
        public Vector4 MiltiplyHorizontalVector(Matrix3D matrix, Vector4 vector)
        {
            double x = vector.X * matrix.M11 + vector.Y * matrix.M21 + vector.Z * matrix.M31 + vector.Fourth * matrix.OffsetX;
            double y = vector.X * matrix.M12 + vector.Y * matrix.M22 + vector.Z * matrix.M32 + vector.Fourth * matrix.OffsetY;
            double z = vector.X * matrix.M13 + vector.Y * matrix.M23 + vector.Z * matrix.M33 + vector.Fourth * matrix.OffsetZ;
            double f = vector.X * matrix.M14 + vector.Y * matrix.M24 + vector.Z * matrix.M34 + vector.Fourth * matrix.M44;
            return new Vector4(x, y, z, f);
        }

        public double Dot(Vector4 vector)
        {
            return X * vector.X + Y * vector.Y + Z * vector.Z + Fourth * vector.Fourth;
        }
        public Vector4 Cross(Vector4 v)
        {
            return new Vector4(Y * v.Z - (Z * v.Y), Z * v.X - (X * v.Z), X * v.Y - (Y * v.Z), 1);
        }
        #region operators

        public static Vector4 operator *(Matrix3D matrix, Vector4 vector)
        {
            return new Vector4(vector.X * matrix.M11 + vector.Y * matrix.M12 + vector.Z * matrix.M13 + vector.Fourth * matrix.M14
                                , vector.X * matrix.M21 + vector.Y * matrix.M22 + vector.Z * matrix.M23 + vector.Fourth * matrix.M24
                                , vector.X * matrix.M31 + vector.Y * matrix.M32 + vector.Z * matrix.M33 + vector.Fourth * matrix.M34
                                , vector.X * matrix.OffsetX + vector.Y * matrix.OffsetY + vector.Z * matrix.OffsetZ + vector.Fourth * matrix.M44);
        }

        public static Vector4 operator *(Vector4 vector, Matrix3D matrix)
        {
            return new Vector4(vector.X * matrix.M11 + vector.Y * matrix.M21 + vector.Z * matrix.M31 + vector.Fourth * matrix.OffsetX
                                , vector.X * matrix.M12 + vector.Y * matrix.M22 + vector.Z * matrix.M32 + vector.Fourth * matrix.OffsetY
                                , vector.X * matrix.M13 + vector.Y * matrix.M23 + vector.Z * matrix.M33 + vector.Fourth * matrix.OffsetZ
                                , vector.X * matrix.M14 + vector.Y * matrix.M24 + vector.Z * matrix.M34 + vector.Fourth * matrix.M44);
        }
       
        public static Vector4 operator /(Vector4 vec,double val)
        {
            return new Vector4(vec.X / val, vec.Y / val, vec.Z / val, vec.Fourth / val);
        }

        public static Vector4 operator *(Vector4 vec, double val)
        {
            return new Vector4(vec.X * val, vec.Y * val, vec.Z * val, vec.Fourth * val);
        }
        public static double operator *(Vector4 vector, Vector4 value)
        {
            return vector.X * value.X + vector.Y * value.Y + vector.Z * value.Z + vector.Fourth * value.Fourth;
        }
        public static Vector4 operator +(Vector4 vector, Vector4 value)
        {
            return new Vector4(vector.X + value.X, vector.Y + value.Y, vector.Z + value.Z, vector.Fourth + value.Fourth);
        }
        public static Vector4 operator -(Vector4 vector, Vector4 value)
        {
            return new Vector4(vector.X - value.X, vector.Y - value.Y, vector.Z - value.Z, vector.Fourth - value.Fourth);
        }
        #endregion
    }
}
