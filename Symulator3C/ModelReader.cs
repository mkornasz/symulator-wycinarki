﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Media.Media3D;
using System.Windows;

namespace Symulator3C
{
    class ModelReader
    {
        static Point3D tmpCenter = new Point3D(0, 0, 0);
        private Point3DHelper helper;
        private List<Point3D> pointsCloud;
        private List<Point3D> points;
        private List<int> pointsId;
        private List<SurfaceM> patches;
        public List<SurfaceM> ModelPatches { get { return patches; } }
        public List<Point3D> CloudPoints { get { return pointsCloud; } }

        public const int BezierSegmentPoints = 3;
        public ModelReader()
        {
            helper = new Point3DHelper();
            pointsCloud = new List<Point3D>();
            points = new List<Point3D>();
            pointsId = new List<int>();
            patches = new List<SurfaceM>();
        }
        public void Read()
        {
            var streamReader = new StreamReader("wiert_inter_NOWY_cZUB.mg1");
            string line;
            while ((line = streamReader.ReadLine()) != null)
            {
                switch (line)
                {
                    case "Point":
                        LoadPoint(streamReader);
                        break;
                    case "BezierSurfaceC0":
                        LoadSurfacem(streamReader, 0);
                        break;
                    case "BezierSurfaceC2":
                        LoadSurfacem(streamReader, 2);
                        break;
                    case "TrimmedCurve":
                        LoadTrimmedCurve(streamReader);
                        break;
                }
            }
            streamReader.Close();

        }


        private void LoadTrimmedCurve(StreamReader streamReader)
        {
            var id = ReadInt(streamReader.ReadLine());
            var name = ReadString(streamReader.ReadLine());
            streamReader.ReadLine();
            var firstSurface = ReadString(streamReader.ReadLine());
            List<double[]> uv1 = ReadUV(streamReader);
            var secondSurface = ReadString(streamReader.ReadLine());

            uv1 = uv1.Distinct().ToList();

            List<Tuple<Point3D, double[]>> tmp = new List<Tuple<Point3D, double[]>>();
            var patch = patches.Find(p => p.Name == firstSurface);
            foreach (var param in uv1)
                tmp.Add(Tuple.Create<Point3D, double[]>(patch.CalculatePointOnSurface((int)param[0], (int)param[1], param[2], param[3]), param));

            if (secondSurface != "0")
            {
                SetCenter(tmp);
                if (firstSurface == "glowna" && (secondSurface == "przednia" || secondSurface == "tylnia"))
                    tmp.Sort(SortClockwiseHorizontal);
                else
                    tmp.Sort(SortClockwise);
            }
            if (patch != null)
            {
                patch.IntersectionUV.Add(tmp);
                patch.IntersectionWith.Add(patches.Find(p => p.Name == secondSurface));
            }

            List<double[]> uv2 = ReadUV(streamReader);
            if (secondSurface == "0")
                return;

            uv2 = uv2.Distinct().ToList();

            tmp = new List<Tuple<Point3D, double[]>>();
            var patch2 = patches.Find(p => p.Name == secondSurface);
            foreach (var param in uv2)
                tmp.Add(Tuple.Create<Point3D, double[]>(patch2.CalculatePointOnSurface((int)param[0], (int)param[1], param[2], param[3]), param));

            SetCenter(tmp);
            if (firstSurface == "glowna" && (secondSurface == "przednia" || secondSurface == "tylnia"))
                tmp.Sort(SortClockwiseHorizontal);
            else
                tmp.Sort(SortClockwise);
            if (patch2 != null)
            {
                patch2.IntersectionUV.Add(tmp);
                patch2.IntersectionWith.Add(patches.Find(p => p.Name == firstSurface));
            }
        }
        private void LoadSurfacem(StreamReader streamReader, int continuity)
        {
            var id = ReadInt(streamReader.ReadLine());
            var name = ReadString(streamReader.ReadLine());
            var width = ReadDouble(streamReader.ReadLine());
            var height = ReadDouble(streamReader.ReadLine());
            var horizontalPatches = ReadInt(streamReader.ReadLine());
            var verticalPatches = ReadInt(streamReader.ReadLine());
            var isCylindrical = ReadBool(streamReader.ReadLine());
            var x = ReadDouble(streamReader.ReadLine());
            var y = ReadDouble(streamReader.ReadLine());
            var z = ReadDouble(streamReader.ReadLine());
            var matrix = ReadMatrix(streamReader);
            var colorName = ReadString(streamReader.ReadLine());
            List<Point3D> surfacePoints = ReadPoints(streamReader);
            SurfaceM patch = null;

            int verticalPoints = continuity == 0 ? verticalPatches * BezierSegmentPoints + 1 : verticalPatches + BezierSegmentPoints;
            int horizontalPoints = isCylindrical ? (continuity == 0 ? horizontalPatches * BezierSegmentPoints : horizontalPatches)
                : (continuity == 0 ? horizontalPatches * BezierSegmentPoints + 1 : BezierSegmentPoints + horizontalPatches);
            var p = new Point3D[verticalPoints, horizontalPoints];
            int index = 0;
            for (int i = 0; i < p.GetLength(0); i++)
                for (int j = 0; j < p.GetLength(1); j++)
                {
                    Vector4 tmp = new Vector4(surfacePoints[index].X, surfacePoints[index].Y, surfacePoints[index++].Z);
                    p[i, j] = new Point3D(tmp.X, tmp.Y, tmp.Z);
                }

            switch (continuity)
            {
                case 0:
                    patch = new SurfaceC0(x, y, z, name, isCylindrical, width, height, verticalPatches, horizontalPatches, p);
                    break;
                case 2:
                    patch = new SurfaceC2(x, y, z, name, isCylindrical, width, height, verticalPatches, horizontalPatches, p);
                    break;
            }
            patches.Add(patch);
        }
        private void LoadPoint(StreamReader streamReader)
        {
            var id = ReadInt(streamReader.ReadLine());
            var name = ReadString(streamReader.ReadLine());
            var x = ReadDouble(streamReader.ReadLine());
            var y = ReadDouble(streamReader.ReadLine());
            var z = ReadDouble(streamReader.ReadLine());
            var matrix = ReadMatrix(streamReader);
            var colorName = ReadString(streamReader.ReadLine());
            Vector4 tmp = new Vector4(x, y, z);
            tmp = matrix * tmp;
            tmp = MatrixScale(0.12f) * tmp;
            Point3D p = new Point3D(tmp.X, tmp.Y, tmp.Z + 2);
            points.Add(p);
            pointsId.Add(id);
            streamReader.ReadLine();
        }
        private List<Point3D> ReadPoints(StreamReader streamReader)
        {
            List<Point3D> potentialPoints = new List<Point3D>();
            while (true)
            {
                var line = streamReader.ReadLine();
                if (string.IsNullOrEmpty(line)) break;
                var id = ReadInt(line);
                for (int i = 0; i < points.Count; i++)
                    if (pointsId[i] == id)
                        potentialPoints.Add(points[i]);
            }
            return potentialPoints;
        }
        private List<double[]> ReadUV(StreamReader streamReader)
        {
            List<double[]> points = new List<double[]>();
            while (true)
            {
                var line = streamReader.ReadLine();
                if (string.IsNullOrEmpty(line))
                    break;
                if (line == "UV2") break;
                var u = ReadDouble(line);
                var v = ReadDouble(streamReader.ReadLine());
                var i = ReadDouble(streamReader.ReadLine());
                var j = ReadDouble(streamReader.ReadLine());
                points.Add(new double[] { i, j, u, v });
            }
            return points;
        }
        private Matrix3D ReadMatrix(StreamReader streamReader)
        {
            streamReader.ReadLine();
            string data = streamReader.ReadLine() + " ";
            data = data + streamReader.ReadLine() + " ";
            data = data + streamReader.ReadLine() + " ";
            data = data + streamReader.ReadLine();
            return Matrix3D.Parse(data.Replace(',', '.'));
        }
        private double ReadDouble(string line)
        {
            return double.Parse(line.Substring(line.IndexOf("=", System.StringComparison.Ordinal) + 1));
        }
        private string ReadString(string line)
        {
            return line.Substring(line.IndexOf("=", System.StringComparison.Ordinal) + 1);
        }
        private int ReadInt(string line)
        {
            return int.Parse(line.Substring(line.IndexOf("=", System.StringComparison.Ordinal) + 1));
        }
        private bool ReadBool(string line)
        {
            return bool.Parse(line.Substring(line.IndexOf("=", System.StringComparison.Ordinal) + 1));
        }


        private void SetCenter(List<Tuple<Point3D, double[]>> surfaceIntersection)
        {
            Point3D center = new Point3D(0, 0, 0);

            for (int i = 0; i < surfaceIntersection.Count; i++)
            {
                center = new Point3D(0, 0, 0);
                foreach (var p in surfaceIntersection)
                {
                    center.X += p.Item1.X;
                    center.Y += p.Item1.Y;
                    center.Z += p.Item1.Z;
                }
                center.X /= surfaceIntersection.Count;
                center.Y /= surfaceIntersection.Count;
                center.Z /= surfaceIntersection.Count;
            }
            tmpCenter = center;
        }
        private static int SortClockwise(Tuple<Point3D, double[]> A, Tuple<Point3D, double[]> B)
        {
            double aTanA, aTanB;
            aTanA = Math.Atan2(A.Item1.Y - tmpCenter.Y, A.Item1.X - tmpCenter.X);
            aTanB = Math.Atan2(B.Item1.Y - tmpCenter.Y, B.Item1.X - tmpCenter.X);

            if (aTanA < aTanB) return -1;
            else if (aTanA > aTanB) return 1;
            return 0;
        }
        private static int SortClockwiseHorizontal(Tuple<Point3D, double[]> A, Tuple<Point3D, double[]> B)
        {
            double aTanA, aTanB;
            aTanA = Math.Atan2(A.Item1.X - tmpCenter.X, A.Item1.Z - tmpCenter.Z);
            aTanB = Math.Atan2(B.Item1.X - tmpCenter.X, B.Item1.Z - tmpCenter.Z);

            if (aTanA < aTanB) return -1;
            else if (aTanA > aTanB) return 1;
            return 0;
        }
        private Matrix3D MatrixScale(float ratio)
        {
            return new Matrix3D(ratio, 0, 0, 0, 0, ratio, 0, 0, 0, 0, ratio, 0, 0, 0, 0, 1);
        }
        private Matrix3D MatrixTranslate(Point3D vector)
        {
            return new Matrix3D(1, 0, 0, vector.X, 0, 1, 0, vector.Y, 0, 0, 1, vector.Z, 0, 0, 0, 1);
        }
        public void GetCloud()
        {
            pointsCloud.Clear();
            foreach (var p in patches)
                for (int i = 0; i < p.VerticalPatches; i++)
                    for (int j = 0; j < p.HorizontalPatches; j++)
                        for (double u = 0; u < 1; u += 0.025)
                            for (double v = 0; v < 1; v += 0.025)
                                pointsCloud.Add(p.CalculatePointOnSurface(i, j, u, v));
        }

        public List<Point3D> GetIntersections()
        {
            List<Point3D> tmp = new List<Point3D>();
            pointsCloud.Clear();
            foreach (var p in patches)
                for (int i = 0; i < p.IntersectionUV.Count; i++)
                    for (int j = 0; j < p.IntersectionUV[i].Count; j++)
                    {
                        var uv = p.IntersectionUV[i][j].Item2;
                        tmp.Add(p.CalculatePointOnSurface((int)uv[0], (int)uv[1], uv[2], uv[3]));
                    }

            return tmp;
        }
    }
}
