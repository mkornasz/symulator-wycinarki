﻿using System;
using System.Windows;
using System.Windows.Media.Media3D;

namespace Symulator3C
{
    class Point3DHelper
    {
        public Point3D CalculateCross(Point3D a, Point3D b)
        {
            double x = (a.Y * b.Z) - (a.Z * b.Y);
            double y = (a.Z * b.X) - (a.X * b.Z);
            double z = (a.X * b.Y) - (a.Y * b.X);
            return new Point3D(x, y, z);
        }
        public double Distance(Point3D a, Point3D b)
        {
            return Math.Sqrt((b.X - a.X) * (b.X - a.X) + (b.Y - a.Y) * (b.Y - a.Y) + (b.Z - a.Z) * (b.Z - a.Z));
        }
        public double Distance(SharpGL.SceneGraph.Vertex a, Point3D b)
        {
            return Math.Sqrt((b.X - a.X) * (b.X - a.X) + (b.Y - a.Y) * (b.Y - a.Y) + (b.Z - a.Z) * (b.Z - a.Z));
        }
        public Point3D Multiple(Point3D a, double b)
        {
            return new Point3D(a.X * b, a.Y * b, a.Z * b);
        }

        public double Length(Point3D vector)
        {
            return Math.Sqrt(vector.X * vector.X + vector.Y * vector.Y + vector.Z * vector.Z);
        }
        public Point3D Normalize(Point3D a)
        {
            double l = Length(a);
            return new Point3D(a.X / l, a.Y / l, a.Z / l);
        }
        public double DotProduct2(Point a, Point b)
        {
            return a.X * b.X + a.Y * b.Y;
        }
        public Point Multiple2(Point a, double b)
        {
            return new Point(a.X * b, a.Y * b);
        }
        public double Length2(Point vector)
        {
            return Math.Sqrt(vector.X * vector.X + vector.Y * vector.Y);
        }
        public Point Normalize2(Point a)
        {
            double l = Length2(a);
            return new Point(a.X / l, a.Y / l);
        }
        public Point3D Add(Point3D a, Point3D b)
        {
            return new Point3D(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
        }
        public Point3D Minus(Point3D a, Point3D b)
        {
            return new Point3D(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
        }

        public Point3D GetNormal(SurfaceM s, int i, int j, double u, double v, double radius)
        {
            var tmp = s.CalculatePointOnSurface(i, j, u, v);
            var tmpU = s.CalculatePointOnSurface(i, j, u, v, true, false);
            var tmpV = s.CalculatePointOnSurface(i, j, u, v, false, true);
            var a = CalculateCross(tmpU, tmpV);
            var b = Normalize(a);
            b = Multiple(b, radius);
            var c = Add(b, tmp);

            return c;
        }

        public Point3D GetTangent(SurfaceM s, int i, int j, double u, double v, double radius, bool uDirection)
        {
            Point3D tmp0 = s.CalculatePointOnSurface(i, j, u, v);
            Point3D tmp = new Point3D();
            if (uDirection)
                tmp = s.CalculatePointOnSurface(i, j, u, v, true, false);
            else
                tmp = s.CalculatePointOnSurface(i, j, u, v, false, true);
            var b = Normalize(tmp);
            b = Multiple(b, -radius);
            var c = Add(b, tmp0);

            return c;
        }
    }
}
