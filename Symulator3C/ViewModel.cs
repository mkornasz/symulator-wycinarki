﻿using System.Collections.Generic;
using Microsoft.Practices.Prism.Commands;
using System.Windows.Input;
using SharpGL;
using SharpGL.SceneGraph;
using System.Windows;
using System.Windows.Threading;
using System;
using System.Windows.Media.Media3D;

namespace Symulator3C
{
    class ViewModel : ViewModelBase
    {

        ModelReader model;
        PathWriter writer;
        List<Point3D> cloudPoints, outter, intersections;
        #region Variables
        private DispatcherTimer _simulationTimer;
        private string _pathFileName;
        private OpenGL gl;
        private bool _isMouseDown, _isCutterFlat, _startEnable, _startNotEnable, _isPause, _showPath = true, _isInitialized = false;
        private int _lenghtX, _lenghtZ, _divideX, _divideY, _moveCounter;
        private float _rotateHor, _rotateVer, _scale, _minY, deltaX, deltaZ, _lx, _ly, _lz, _startX, _startZ;
        private double _oldMouseX, _oldMouseY, _mouseX, _mouseY, _radius, _lenghtY;
        private float[,] HighMap;
        private Vertex _cutterCoord, _cutterCoordNext, _cutterDiff;
        private Vertex[] array;
        private List<Vertex> _cutterPositions;
        private List<Vertex>[,] bottoms, tops, left, right, front, back;
        private List<List<Vertex>>[,] sides;
        private float[] normals;
        private List<uint> iindices;
        private PathParser _parser;
        private IntPtr ptrBall, ptrCylinder;

        private float _velocity;
        int squares;
        #endregion
        #region Properties
        public float Velocity
        {
            get { return _velocity; }
            set
            {
                if (_velocity != value)
                {
                    _velocity = value;
                    OnPropertyChanged("Velocity");
                    OnPropertyChanged("VelocityString");
                }
            }
        }
        public string VelocityString
        {
            get { return "V= " + _velocity.ToString(); }
        }
        public bool StartEnable
        {
            get { return _startEnable; }
            set
            {
                if (_startEnable != value)
                {
                    _startEnable = value;
                    PauseEnable = !value;
                    OnPropertyChanged("StartEnable");
                }
            }
        }
        public bool PauseEnable
        {
            get { return _startNotEnable; }
            set
            {
                if (_startNotEnable != value)
                {
                    _startNotEnable = value;
                    OnPropertyChanged("PauseEnable");
                }
            }
        }
        public bool IsPause
        {
            get { return _isPause; }
            set
            {
                if (_isPause != value)
                {
                    _isPause = value;
                    OnPropertyChanged("IsPause");
                }
            }
        }
        public bool ShowPath
        {
            get { return _showPath; }
            set
            {
                if (_showPath != value)
                {
                    _showPath = value;
                    OnPropertyChanged("ShowPath");
                }
            }
        }
        public string PathFileName
        {
            get { return _pathFileName; }
            set
            {
                if (_pathFileName != value)
                {
                    _pathFileName = value;
                    OnPropertyChanged("PathFileName");
                }
            }
        }
        public string DivideX
        {
            get { return _divideX.ToString(); }
            set
            {
                if (_divideX.ToString() != value)
                {
                    if (value.Length != 0)
                        _divideX = int.Parse(value);
                    else
                        _divideX = 0;
                    OnPropertyChanged("PhiZero");
                }
            }
        }
        public string DivideY
        {
            get { return _divideY.ToString(); }
            set
            {
                if (_divideY.ToString() != value)
                {
                    if (value.Length != 0)
                        _divideY = int.Parse(value);
                    else
                        _divideY = 0;
                    OnPropertyChanged("DivideY");
                }
            }
        }
        public string LenghtX
        {
            get { return _lenghtX.ToString(); }
            set
            {
                if (_lenghtX.ToString() != value)
                {
                    if (value.Length != 0)
                        _lenghtX = int.Parse(value);
                    else
                        _lenghtX = 0;
                    OnPropertyChanged("LenghtX");
                }
            }
        }
        public string LenghtY
        {
            get { return _lenghtY.ToString(); }
            set
            {
                if (_lenghtY.ToString() != value)
                {
                    if (value.Length != 0)
                        _lenghtY = double.Parse(value);
                    else
                        _lenghtY = 0;
                    OnPropertyChanged("LenghtY");
                }
            }
        }
        public string LenghtZ
        {
            get { return _lenghtZ.ToString(); }
            set
            {
                if (_lenghtZ.ToString() != value)
                {
                    if (value.Length != 0)
                        _lenghtZ = int.Parse(value);
                    else
                        _lenghtZ = 0;
                    OnPropertyChanged("LenghtZ");
                }
            }
        }
        public string MinY
        {
            get { return _minY.ToString(); }
            set
            {
                if (_minY.ToString() != value)
                {
                    if (value.Length != 0)
                        _minY = float.Parse(value);
                    else
                        _minY = 0;
                    OnPropertyChanged("MinY");
                }
            }
        }
        #endregion       
        #region Commands
        private ICommand _openGLDrawExecutedCommand, _mouseDownExecutedCommand, _mouseUpExecutedCommand, _mouseMoveExecutedCommand, _mouseWheelExecutedCommand,
            _startExecutedCommand, _pauseExecutedCommand, _stopExecutedCommand, _resetExecutedCommand, _loadFileExecutedCommand, _comboboxExecutedCommand,
            _keyDownExecutedCommand, _showModelExecutedCommand, _obrobkaWstepnaExecutedCommand, _obrobkaDokladnaObrysExecutedCommand, _obrobkaDodatkowaExecutedCommand;
        public ICommand KeyDown
        {
            get { return _keyDownExecutedCommand ?? (_keyDownExecutedCommand = new DelegateCommand(KeyDownExecuted)); }
        }
        public ICommand DrawCommand
        {
            get { return _openGLDrawExecutedCommand ?? (_openGLDrawExecutedCommand = new DelegateCommand(OpenGLDrawExecuted)); }
        }
        public ICommand MouseDownCommand
        {
            get { return _mouseDownExecutedCommand ?? (_mouseDownExecutedCommand = new DelegateCommand(MouseDownExecuted)); }
        }
        public ICommand MouseUpCommand
        {
            get { return _mouseUpExecutedCommand ?? (_mouseUpExecutedCommand = new DelegateCommand(MouseUpExecuted)); }
        }
        public ICommand MouseMoveCommand
        {
            get { return _mouseMoveExecutedCommand ?? (_mouseMoveExecutedCommand = new DelegateCommand(MouseMoveExecuted)); }
        }
        public ICommand MouseWheelCommand
        {
            get { return _mouseWheelExecutedCommand ?? (_mouseWheelExecutedCommand = new DelegateCommand(MouseWheelExecuted)); }
        }
        public ICommand StartCommand
        {
            get { return _startExecutedCommand ?? (_startExecutedCommand = new DelegateCommand(StartCommandExecuted)); }
        }
        public ICommand PauseCommand
        {
            get { return _pauseExecutedCommand ?? (_pauseExecutedCommand = new DelegateCommand(PauseCommandExecuted)); }
        }
        public ICommand StopCommand
        {
            get { return _stopExecutedCommand ?? (_stopExecutedCommand = new DelegateCommand(StopCommandExecuted)); }
        }
        public ICommand ResetCommand
        {
            get { return _resetExecutedCommand ?? (_resetExecutedCommand = new DelegateCommand(ResetCommandExecuted)); }
        }
        public ICommand LoadFileCommand
        {
            get { return _loadFileExecutedCommand ?? (_loadFileExecutedCommand = new DelegateCommand(LoadFileCommandExecuted)); }
        }
        public ICommand ComboboxCommand
        {
            get { return _comboboxExecutedCommand ?? (_comboboxExecutedCommand = new DelegateCommand(CutterComboExecuted)); }
        }
        public ICommand ObrobkaWstepnaCommand
        {
            get { return _obrobkaWstepnaExecutedCommand ?? (_obrobkaWstepnaExecutedCommand = new DelegateCommand(ObrobkaWstepnaCommandExecuted)); }
        }
        public ICommand ObrobkaDokladnaObrysCommand
        {
            get { return _obrobkaDokladnaObrysExecutedCommand ?? (_obrobkaDokladnaObrysExecutedCommand = new DelegateCommand(ObrobkaDokladnaObrysCommandExecuted)); }
        }
        public ICommand ShowModelCommand
        {
            get { return _showModelExecutedCommand ?? (_showModelExecutedCommand = new DelegateCommand(ShowModelCommandExecuted)); }
        }
        public ICommand ObrobkaDodatkowaCommand
        {
            get { return _obrobkaDodatkowaExecutedCommand ?? (_obrobkaDodatkowaExecutedCommand = new DelegateCommand(ObrobkaDodatkowaCommandExecuted)); }
        }
        private void ShowModelCommandExecuted()
        {
            model = new ModelReader();
            model.Read();
            model.GetCloud();
            cloudPoints = new List<Point3D>(model.CloudPoints);
            intersections = model.GetIntersections();
        }
        private void ObrobkaWstepnaCommandExecuted()
        {
            if (writer == null)
                writer = new PathWriter(tops, _lenghtX, _lenghtZ);

            _isCutterFlat = false;
            _radius = 0.8;

            List<Vertex> list = writer.CutFirstTime(HighMap, cloudPoints, (float)_radius);
            _cutterPositions = new List<Vertex>(list);
            _cutterCoord = new Vertex(_cutterPositions[0]);
            _moveCounter = 1;
            _cutterCoordNext = _cutterCoord;


            if (_parser == null)
                _parser = new PathParser();
            _parser.Write(list, "jeden.k16");
        }
        private void ObrobkaDokladnaObrysCommandExecuted()
        {
            _radius = 0.6;
            _isCutterFlat = true;
            if (writer == null)
                writer = new PathWriter(tops, _lenghtX, _lenghtZ);
            outter = writer.CutBorder(model.ModelPatches, _radius);

            List<Vertex> list = new List<Vertex>();
            foreach (var p in outter)
                list.Add(new Vertex((float)p.X, (float)p.Z+0.02f, (float)p.Y));

            List<Vertex> list2 = writer.CutSecondTimeFlat(outter);
            list.AddRange(list2);
            _cutterPositions = new List<Vertex>(list);
            _cutterCoord = new Vertex(_cutterPositions[0]);
            _moveCounter = 1;
            _cutterCoordNext = _cutterCoord;

            if (_parser == null)
                _parser = new PathParser();
            _parser.Write(list, "dwa.f12");
        }
        private void ObrobkaDodatkowaCommandExecuted()
        {
            _radius = 0.4;
            _isCutterFlat = false;
            if (writer == null)
                writer = new PathWriter(tops, _lenghtX, _lenghtZ);
            List<Vertex> list = writer.CutThirdTime(model.ModelPatches, _radius);
            if (list.Count == 0)
                return;
            _cutterPositions = new List<Vertex>(list);
            _cutterCoord = new Vertex(_cutterPositions[0]);
            _moveCounter = 1;
            _cutterCoordNext = _cutterCoord;

            if (_parser == null)
                _parser = new PathParser();
            _parser.Write(list, "trzy.k08");
        }
        private void StartCommandExecuted()
        {
            if (_cutterPositions == null)
            {
                MessageBox.Show("Nie wybrano pliku.");
                return;
            }
            IsPause = false;
            StartEnable = false;
            _simulationTimer.Start();
        }
        private void PauseCommandExecuted()
        {
            IsPause = !IsPause;
            if (IsPause)
                _simulationTimer.Stop();
            else
                _simulationTimer.Start();
        }
        private void StopCommandExecuted()
        {
            if (_cutterPositions == null)
            {
                MessageBox.Show("Nie wybrano pliku.");
                return;
            }
            _simulationTimer.Stop();
            CutWithoutView();
            BuildCutterVisualization();
            OpenGLDrawExecuted();
            MessageBox.Show("Symulacja zakonczona.");
            StartEnable = true;
            _moveCounter = 1;
        }
        private void ResetCommandExecuted()
        {
            _simulationTimer.Stop();
            _moveCounter = 0;
            BuildCube();
            PrepareVertexArray();
            StartEnable = true;
        }
        private void LoadFileCommandExecuted()
        {
            _parser = new PathParser();
            _parser.InitReader();
            if (!_parser.IsParserOk)
                return;
            PathFileName = _parser.FileName;
            _isCutterFlat = _parser.IsCutterFlat;
            _radius = 0.5 * _parser.CutterRadius * 0.1;
            _parser.Read();
            _cutterPositions = new List<Vertex>(_parser.CutterPositions);
            _cutterCoord = new Vertex(_cutterPositions[0]);
            _moveCounter = 1;
            _cutterCoordNext = _cutterCoord;
            OnPropertyChanged("AllMoves");
            OnPropertyChanged("Move");
        }
        private void MouseDownExecuted()
        {
            _isMouseDown = true;
            Keyboard.ClearFocus();
            Keyboard.Focus((Application.Current.MainWindow as MainWindow).ProgramWindow);
            (Application.Current.MainWindow as MainWindow).ProgramWindow.Focus();

        }
        private void MouseUpExecuted()
        {
            _isMouseDown = false;
        }
        private void MouseWheelExecuted()
        {
            int a = Mouse.MouseWheelDeltaForOneLine / 5;
            float numSteps = Mouse.RightButton == MouseButtonState.Pressed ? -a / 15 : a / 15;
            if (_scale + numSteps / 50 > 0)
                _scale += numSteps / 50;
        }
        private void MouseMoveExecuted()
        {
            if (!_isMouseDown) return;
            _oldMouseX = _mouseX;
            _oldMouseY = _mouseY;

            Point mouseXY = Mouse.GetPosition((Application.Current.MainWindow as MainWindow).OpenGlContr);
            _mouseX = mouseXY.X;
            _mouseY = mouseXY.Y;

            if (_mouseY < 60)
                _mouseY = 60;
            if (_mouseY > 450)
                _mouseY = 450;

            if ((_mouseX - _oldMouseX) > 0)       // mouse moved to the right
                _rotateHor += 3.0f;
            else if ((_mouseX - _oldMouseX) < 0)  // mouse moved to the left
                _rotateHor -= 3.0f;

            if ((_mouseY - _oldMouseY) > 0)       // mouse moved to the right
                _rotateVer += 3.0f;
            else if ((_mouseY - _oldMouseY) < 0)  // mouse moved to the left
                _rotateVer -= 3.0f;
        }
        private void CutterComboExecuted()
        {
            var index = (Application.Current.MainWindow as MainWindow).Combo.SelectedIndex;
            switch (index)
            {
                case 0:
                    _radius = 1.6 / 2;
                    _isCutterFlat = false;
                    break;
                case 1:
                    _radius = 1.2 / 2;
                    _isCutterFlat = false;
                    break;
                case 2:
                    _radius = 1.0 / 2;
                    _isCutterFlat = false;
                    break;
                case 3:
                    _radius = 0.8 / 2;
                    _isCutterFlat = false;
                    break;
                case 4:
                    _radius = 0.2 / 2;
                    _isCutterFlat = false;
                    break;
                case 5:
                    _radius = 0.1 / 2;
                    _isCutterFlat = false;
                    break;
                case 6:
                    _radius = 1.0 / 2;
                    _isCutterFlat = true;
                    break;
                case 7:
                    _radius = 1.2 / 2;
                    _isCutterFlat = true;
                    break;
            }
        }
        private void KeyDownExecuted()
        {
            if (Keyboard.IsKeyDown(Key.Down))
            {
                if (Keyboard.IsKeyDown(Key.RightCtrl))
                    _ly += 0.05f;

                else
                    _lz += 0.05f;
            }
            if (Keyboard.IsKeyDown(Key.Up))
            {
                if (Keyboard.IsKeyDown(Key.RightCtrl))
                    _ly -= 0.05f;
                else
                    _lz -= 0.05f;
            }
            if (Keyboard.IsKeyDown(Key.Left))
                _lx -= 0.05f;
            if (Keyboard.IsKeyDown(Key.Right))
                _lx += 0.05f;


            OpenGLDrawExecuted();
        }
        #endregion

        public ViewModel()
        {
            CreateObjects();
            InitializeVariables();

            ShowModelCommandExecuted();
        }
        private void CreateObjects()
        {
            cloudPoints = new List<Point3D>();
            outter = new List<Point3D>();
            gl = new OpenGL();
            _cutterDiff = new Vertex(0, 0, 0);
            _cutterCoord = _cutterCoordNext = new Vertex(0, 6.5f, 0);
            ptrBall = gl.NewQuadric();
            ptrCylinder = gl.NewQuadric();
            _simulationTimer = new DispatcherTimer { Interval = new TimeSpan(0, 0, 0, 0, 1) };
            _simulationTimer.Tick += SimulationTimerTick;
        }
        private void InitializeVariables()
        {
            _velocity = 0.01f;
            _lx = 7.0f;
            _ly = 6.1f;
            _lz = 0.0f;
            _pathFileName = "";
            _scale = 0.1f;
            _minY = -3;
            _lenghtX = 15;
            _lenghtZ = 15;
            _lenghtY = 2;
            _divideX = _divideY = 200;
            _moveCounter = 0;
            _radius = 0.2;
            _mouseX = _mouseY = 0;
            _rotateHor = _rotateVer = 0.0f;
            _isPause = _startEnable = true;
            _isMouseDown = false;
        }
        private void OpenGLDrawExecuted()
        {
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -3.0f);
            gl.Rotate(_rotateVer, _rotateHor, 0.0f);
            gl.Scale(_scale, _scale, _scale);

            BuildAxes();

            if (!_isInitialized)
            {
                BuildCube();
                PrepareVertexArray();
                gl.Enable(OpenGL.GL_DEPTH_TEST);


                float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
                float[] light0pos = new float[] { 7.0f, 0.0f, 7.0f, 1.0f };
                // float[] light1pos = new float[] { 7.0f, -7.0f, 0.0f, 1.0f };
                float[] light0ambient = new float[] { 0.0f, 0.0f, 0.9f, 1.0f };
                float[] light1ambient = new float[] { 1.0f, 0.0f, 0.0f, 1.0f };
                float[] light0diffuse = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
                float[] light1diffuse = new float[] { 1.0f, 1.0f, 1.0f, 1.0f };
                float[] light0specular = new float[] { 0.1f, 0.1f, 0.1f, 1.0f };

                float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
                gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

                gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
                gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
                gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
                gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
                gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);

                gl.Light(OpenGL.GL_LIGHT1, OpenGL.GL_POSITION, light0pos);
                gl.Light(OpenGL.GL_LIGHT1, OpenGL.GL_AMBIENT, light1ambient);
                gl.Light(OpenGL.GL_LIGHT1, OpenGL.GL_DIFFUSE, light1diffuse);
                gl.Light(OpenGL.GL_LIGHT1, OpenGL.GL_SPECULAR, light0specular);

                gl.Enable(OpenGL.GL_LIGHTING);
                gl.Enable(OpenGL.GL_LIGHT0);
                gl.ShadeModel(OpenGL.GL_SMOOTH);
                _isInitialized = true;
            }

            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, new float[] { _lx, _ly, _lz, 1.0f });

            BuildCutterVisualization();
            if (ShowPath && _cutterPositions != null)
                ShowPathPoints(_cutterPositions);
            DrawCube();
        //    DisplayModelPoints();
        }
        private void ChangeVertexValue(int i, int j, int k, int tabIndex, Vertex newVertex)
        {
            var bigCount = _divideX * _divideY * 4 * tabIndex;
            var smallCount = (4 * (_divideX * i + j)) + k;
            array[bigCount + smallCount] = newVertex;
        }
        private void BuildCutterVisualization()
        {
            gl.Enable(OpenGL.GL_LIGHT1);
            gl.PushMatrix();
            gl.Translate(_cutterCoord.X, _cutterCoord.Y, _cutterCoord.Z);

            if (!_isCutterFlat)
                gl.Translate(0, _radius, 0);
            gl.Rotate(-90, 0, 0);
            if (!_isCutterFlat)
                gl.Sphere(ptrBall, _radius, 15, 15);
            gl.Cylinder(ptrCylinder, _radius, _radius, 6, 15, 15);

            gl.PopMatrix();
            gl.Disable(OpenGL.GL_LIGHT1);
        }
        private void BuildAxes()
        {
            gl.LineWidth(0.01f);
            gl.Begin(OpenGL.GL_LINES);
            gl.Vertex(-10, 0.0, 0.0);
            gl.Vertex(10, 0, 0);
            gl.Vertex(0.0, 0.0, -10);
            gl.Vertex(0.0, 0.0, 10);
            gl.Vertex(0.0, -10.0, 0.0);
            gl.Vertex(0.0, 10.0, 0.0);
            gl.End();
        }
        private void BuildCube()
        {
            bottoms = new List<Vertex>[_divideX, _divideY];
            tops = new List<Vertex>[_divideX, _divideY];
            left = new List<Vertex>[_divideX, _divideY];
            right = new List<Vertex>[_divideX, _divideY];
            front = new List<Vertex>[_divideX, _divideY];
            back = new List<Vertex>[_divideX, _divideY];
            sides = new List<List<Vertex>>[_divideX, _divideY];
            HighMap = new float[_divideX + 1, _divideY + 1];
            for (int i = 0; i < HighMap.GetLength(0); i++)
                for (int j = 0; j < HighMap.GetLength(1); j++)
                    HighMap[i, j] = (float)_lenghtY;

            _startX = -(_lenghtX / 2.0f);
            _startZ = -(_lenghtZ / 2.0f);

            deltaX = (float)_lenghtX / (float)_divideX;
            deltaZ = (float)_lenghtZ / (float)_divideY;

            for (int i = 0; i < _divideX; i++)
                for (int j = 0; j < _divideY; j++)
                {
                    bottoms[i, j] = new List<Vertex>()
                      {
                         new Vertex(_startX+ i *deltaX,    0,_startZ+j*deltaZ),
                         new Vertex(_startX+(i+1)*deltaX,  0,_startZ+j*deltaZ),
                         new Vertex(_startX+ (i+1)*deltaX, 0,_startZ+(j+1)*deltaZ),
                         new Vertex(_startX+ i *deltaX,    0,_startZ+(j+1)*deltaZ)
                       };

                    tops[i, j] = new List<Vertex>()
                      {
                         new Vertex(_startX+ i *deltaX,   HighMap[i,j],_startZ+j*deltaZ),
                         new Vertex(_startX+(i+1)*deltaX, HighMap[i+1,j],_startZ+j*deltaZ),
                         new Vertex(_startX+ (i+1)*deltaX,HighMap[i+1,j+1],_startZ+(j+1)*deltaZ),
                         new Vertex(_startX+ i *deltaX,   HighMap[i,j+1],_startZ+(j+1)*deltaZ)
                       };


                    left[i, j] = new List<Vertex>()
                    {
                         new Vertex(bottoms[i, j][0].X, bottoms[i,j][0].Y, bottoms[i, j][0].Z),
                            new Vertex(bottoms[i, j][1].X, bottoms[i,j][1].Y, bottoms[i, j][1].Z),
                            new Vertex(bottoms[i, j][1].X, tops[i,j][1].Y,    bottoms[i, j][1].Z),
                            new Vertex(bottoms[i, j][0].X, tops[i,j][0].Y,    bottoms[i, j][0].Z)
                    };
                    right[i, j] = new List<Vertex>()
                    {
                         new Vertex(bottoms[i, j][0].X,  bottoms[i,j][0].Y,  bottoms[i, j][0].Z),
                            new Vertex(bottoms[i, j][3].X,  bottoms[i,j][3].Y,  bottoms[i, j][3].Z),
                            new Vertex(bottoms[i, j][3].X,  tops[i,j][3].Y,   bottoms[i, j][3].Z),
                            new Vertex(bottoms[i, j][0].X,  tops[i,j][0].Y,   bottoms[i, j][0].Z)
                    };
                    front[i, j] = new List<Vertex>()
                    {
                        new Vertex(bottoms[i, j][2].X, bottoms[i,j][2].Y, bottoms[i, j][2].Z),
                            new Vertex(bottoms[i, j][1].X, bottoms[i,j][1].Y, bottoms[i, j][1].Z),
                            new Vertex(bottoms[i, j][1].X, tops[i,j][1].Y,    bottoms[i, j][1].Z),
                            new Vertex(bottoms[i, j][2].X, tops[i,j][2].Y,    bottoms[i, j][2].Z)
                    };
                    back[i, j] = new List<Vertex>()
                    {
                         new Vertex(bottoms[i, j][2].X,  bottoms[i,j][2].Y, bottoms[i, j][2].Z),
                            new Vertex(bottoms[i, j][3].X,  bottoms[i,j][3].Y, bottoms[i, j][3].Z),
                            new Vertex(bottoms[i, j][3].X,  tops[i,j][3].Y,    bottoms[i, j][3].Z),
                            new Vertex(bottoms[i, j][2].X,  tops[i,j][2].Y,    bottoms[i, j][2].Z)
                    };
                }

            List<float> norms = new List<float>();
            iindices = new List<uint>();
            uint counter = 0;
            foreach (var l in bottoms)
            {
                iindices.Add(counter);
                iindices.Add(counter + 1);
                iindices.Add(counter + 2);
                iindices.Add(counter + 2);
                iindices.Add(counter + 3);
                iindices.Add(counter);
                counter += 4;

                foreach (var ll in l)
                {
                    norms.Add(0);
                    norms.Add(-1);
                    norms.Add(0);
                }
            }

            foreach (var l in tops)
            {
                iindices.Add(counter);
                iindices.Add(counter + 1);
                iindices.Add(counter + 2);
                iindices.Add(counter + 2);
                iindices.Add(counter + 3);
                iindices.Add(counter);
                counter += 4;
                foreach (var ll in l)
                {
                    norms.Add(0);
                    norms.Add(1);
                    norms.Add(0);
                }
            }

            foreach (var l in left)
            {
                iindices.Add(counter);
                iindices.Add(counter + 1);
                iindices.Add(counter + 2);
                iindices.Add(counter + 2);
                iindices.Add(counter + 3);
                iindices.Add(counter);
                counter += 4;
                foreach (var ll in l)
                {
                    norms.Add(0);
                    norms.Add(0);
                    norms.Add(-1);
                }
            }
            foreach (var l in right)
            {
                iindices.Add(counter);
                iindices.Add(counter + 1);
                iindices.Add(counter + 2);
                iindices.Add(counter + 2);
                iindices.Add(counter + 3);
                iindices.Add(counter);
                counter += 4;
                foreach (var ll in l)
                {
                    norms.Add(-1);
                    norms.Add(0);
                    norms.Add(0);
                }
            }
            foreach (var l in front)    //OK
            {
                iindices.Add(counter);
                iindices.Add(counter + 1);
                iindices.Add(counter + 2);
                iindices.Add(counter + 2);
                iindices.Add(counter + 3);
                iindices.Add(counter);
                counter += 4;
                foreach (var ll in l)
                {
                    norms.Add(0);
                    norms.Add(1);
                    norms.Add(0);
                }
            }
            foreach (var l in back)
            {
                iindices.Add(counter);
                iindices.Add(counter + 1);
                iindices.Add(counter + 2);
                iindices.Add(counter + 2);
                iindices.Add(counter + 3);
                iindices.Add(counter);
                counter += 4;
                foreach (var ll in l)
                {
                    norms.Add(-1);
                    norms.Add(0);
                    norms.Add(0);
                }
            }

            normals = norms.ToArray();
            squares = 6 * (tops.GetLength(0) * tops.GetLength(1) * 6);
        }

        private void PrepareVertexArray()
        {
            List<Vertex> tmp = new List<Vertex>();

            foreach (var l in bottoms)
                foreach (var ll in l)
                    tmp.Add(ll);

            foreach (var l in tops)
                foreach (var ll in l)
                    tmp.Add(ll);

            foreach (var l in left)
                foreach (var ll in l)
                    tmp.Add(ll);

            foreach (var l in right)
                foreach (var ll in l)
                    tmp.Add(ll);

            foreach (var l in front)
                foreach (var ll in l)
                    tmp.Add(ll);

            foreach (var l in back)
                foreach (var ll in l)
                    tmp.Add(ll);

            array = tmp.ToArray();
        }
        private void DrawCube()
        {
            gl.EnableClientState(OpenGL.GL_NORMAL_ARRAY);
            gl.EnableClientState(OpenGL.GL_VERTEX_ARRAY);
            gl.NormalPointer(OpenGL.GL_FLOAT, 0, normals);
            gl.VertexPointer(3, OpenGL.GL_FLOAT, 0, array);
            gl.DrawElements(OpenGL.GL_TRIANGLES, squares, iindices.ToArray());
            gl.DisableClientState(OpenGL.GL_VERTEX_ARRAY);
            gl.DisableClientState(OpenGL.GL_NORMAL_ARRAY);
        }
        private void ShowPathPoints(List<Vertex> list, int option = 0)
        {
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_TEXTURE);
            if (option > 0)
                gl.Color(1.0f, 1.0f, 0.0f);
            else
                gl.Color(1.0f, 0.0, 0.0);
            if (option > 0)
            {
                gl.PointSize(3);
                gl.Begin(OpenGL.GL_POINTS);
            }
            else
                gl.Begin(OpenGL.GL_LINES);
            for (int i = 0; i < list.Count - 1; i++)
            {
                gl.Vertex(list[i].X, list[i].Y + 0.1, list[i].Z);
                if (option == 0)
                    gl.Vertex(list[i + 1].X, list[i + 1].Y + 0.1, list[i + 1].Z);
            }
            gl.End();
            gl.Enable(OpenGL.GL_LIGHTING);
            gl.Enable(OpenGL.GL_TEXTURE);
        }

        private void CheckCollisions()
        {
            double newHigh = 0;

            deltaX = (float)_lenghtX / (float)_divideX;
            deltaZ = (float)_lenghtZ / (float)_divideY;

            float xLeft = _cutterCoord.X - (float)_radius;
            float xRight = _cutterCoord.X + (float)_radius;
            if ((xRight < _startX && xLeft < _startX) || (xLeft > (-_startX) && xRight > (-_startX)))
                return;
            float zUp = _cutterCoord.Z - (float)_radius;
            float zDown = _cutterCoord.Z + (float)_radius;
            if ((zUp < _startZ && zDown < _startZ) || (zUp > (-_startZ) && zDown > (-_startZ)))
                return;

            int indexXStart = (int)((xLeft - _startX) / deltaX);
            if (indexXStart < 0) indexXStart = 0;
            double xF = ((xRight - _startX) / deltaX);
            int indexXFinish = xF % 10 == 0 ? (int)xF + 1 : (int)xF + 2;
            if (indexXFinish > _divideX) indexXFinish = _divideX;

            int indexZStart = (int)((zUp - _startZ) / deltaZ);
            if (indexZStart < 0) indexZStart = 0;
            double ZF = ((zDown - _startZ) / deltaZ);
            int indexZFinish = ZF % 10 == 0 ? (int)ZF + 1 : (int)ZF + 2;
            if (indexZFinish > _divideY) indexZFinish = _divideY;

            int iHigh, jHigh;
            for (int i = indexXStart; i < indexXFinish; i++)
                for (int j = indexZStart; j < indexZFinish; j++)
                {
                    for (int h = 0; h < 4; h++)
                    {
                        switch (h)
                        {
                            case 0:
                                iHigh = i;
                                jHigh = j;
                                break;
                            case 1:
                                iHigh = i + 1;
                                jHigh = j;
                                break;
                            case 2:
                                iHigh = i + 1;
                                jHigh = j + 1;
                                break;
                            case 3:
                                iHigh = i;
                                jHigh = j + 1;
                                break;
                            default:
                                iHigh = i;
                                jHigh = j;
                                break;
                        }
                        if (_isCutterFlat &&
                            (((tops[i, j][h].X - _cutterCoord.X) * (tops[i, j][h].X - _cutterCoord.X)) + ((tops[i, j][h].Z - _cutterCoord.Z) * (tops[i, j][h].Z - _cutterCoord.Z)) < (_radius * _radius)) &&
                            tops[i, j][h].Y > _cutterCoord.Y)
                            newHigh = _cutterCoord.Y;
                        else
                        {
                            var oldValue = HighMap[iHigh, jHigh] - Math.Abs(bottoms[i, j][h].Y);

                            Vertex a = FindIntersections(tops[i, j][h].X, HighMap[iHigh, jHigh], tops[i, j][h].Z, bottoms[i, j][h].X, bottoms[i, j][h].X, bottoms[i, j][h].Z, _cutterCoord, _radius);
                            if (a.X == -10 && a.Y == -10 && a.Z == -10)
                                newHigh = oldValue;
                            else if (a.Y < bottoms[i, j][h].Y + HighMap[iHigh, jHigh])
                                newHigh = a.Y;
                            else
                                newHigh = oldValue;
                        }

                        if ((float)newHigh == HighMap[iHigh, jHigh])
                            continue;

                        HighMap[iHigh, jHigh] = (float)(Math.Abs(bottoms[i, j][h].Y) + newHigh);
                        HighMap[iHigh, jHigh] = HighMap[iHigh, jHigh] > _lenghtY ? HighMap[iHigh, jHigh] = (float)_lenghtY : HighMap[iHigh, jHigh] < 0 ? HighMap[iHigh, jHigh] = 0 : HighMap[iHigh, jHigh];
                        UpdateVertices(i, j);
                        switch (h)
                        {
                            case 0:
                                UpdateVertices(i, j - 1);
                                UpdateVertices(i - 1, j);
                                UpdateVertices(i - 1, j - 1);
                                break;
                            case 1:
                                UpdateVertices(i - 1, j);
                                UpdateVertices(i - 1, j + 1);
                                UpdateVertices(i, j + 1);
                                break;
                            case 2:
                                UpdateVertices(i + 1, j);
                                UpdateVertices(i, j + 1);
                                UpdateVertices(i + 1, j + 1);
                                break;
                            case 3:
                                UpdateVertices(i + 1, j);
                                UpdateVertices(i + 1, j - 1);
                                UpdateVertices(i, j - 1);
                                break;
                            default:
                                break;
                        }
                    }
                }
        }
        private void UpdateVertices(int i, int j)
        {
            if (i < 0 || j < 0)
                return;
            if (i == tops.GetLength(0) || j == tops.GetLength(1))
                return;
            tops[i, j][0] = new Vertex(tops[i, j][0].X, bottoms[i, j][0].Y + HighMap[i, j], tops[i, j][0].Z);
            tops[i, j][1] = new Vertex(tops[i, j][1].X, bottoms[i, j][0].Y + HighMap[i + 1, j], tops[i, j][1].Z);
            tops[i, j][2] = new Vertex(tops[i, j][2].X, bottoms[i, j][0].Y + HighMap[i + 1, j + 1], tops[i, j][2].Z);
            tops[i, j][3] = new Vertex(tops[i, j][3].X, bottoms[i, j][0].Y + HighMap[i, j + 1], tops[i, j][3].Z);
            left[i, j][2] = new Vertex(bottoms[i, j][1].X, bottoms[i, j][0].Y + HighMap[i + 1, j], bottoms[i, j][1].Z);
            left[i, j][3] = new Vertex(bottoms[i, j][0].X, bottoms[i, j][0].Y + HighMap[i, j], bottoms[i, j][0].Z);
            right[i, j][2] = new Vertex(bottoms[i, j][3].X, bottoms[i, j][0].Y + HighMap[i, j + 1], bottoms[i, j][3].Z);
            right[i, j][3] = new Vertex(bottoms[i, j][0].X, bottoms[i, j][0].Y + HighMap[i, j], bottoms[i, j][0].Z);
            front[i, j][2] = new Vertex(bottoms[i, j][1].X, bottoms[i, j][0].Y + HighMap[i + 1, j], bottoms[i, j][1].Z);
            front[i, j][3] = new Vertex(bottoms[i, j][2].X, bottoms[i, j][0].Y + HighMap[i + 1, j + 1], bottoms[i, j][2].Z);
            back[i, j][2] = new Vertex(bottoms[i, j][3].X, bottoms[i, j][0].Y + HighMap[i, j + 1], bottoms[i, j][3].Z);
            back[i, j][3] = new Vertex(bottoms[i, j][2].X, bottoms[i, j][0].Y + HighMap[i + 1, j + 1], bottoms[i, j][2].Z);


            ChangeVertexValue(i, j, 0, (int)TabNames.Tops, new Vertex(tops[i, j][0].X, bottoms[i, j][0].Y + HighMap[i, j], tops[i, j][0].Z));
            ChangeVertexValue(i, j, 1, (int)TabNames.Tops, new Vertex(tops[i, j][1].X, bottoms[i, j][0].Y + HighMap[i + 1, j], tops[i, j][1].Z));
            ChangeVertexValue(i, j, 2, (int)TabNames.Tops, new Vertex(tops[i, j][2].X, bottoms[i, j][0].Y + HighMap[i + 1, j + 1], tops[i, j][2].Z));
            ChangeVertexValue(i, j, 3, (int)TabNames.Tops, new Vertex(tops[i, j][3].X, bottoms[i, j][0].Y + HighMap[i, j + 1], tops[i, j][3].Z));
            ChangeVertexValue(i, j, 2, (int)TabNames.Left, new Vertex(bottoms[i, j][1].X, tops[i, j][1].Y, bottoms[i, j][1].Z));
            ChangeVertexValue(i, j, 3, (int)TabNames.Left, new Vertex(bottoms[i, j][0].X, tops[i, j][0].Y, bottoms[i, j][0].Z));
            ChangeVertexValue(i, j, 2, (int)TabNames.Right, new Vertex(bottoms[i, j][3].X, tops[i, j][3].Y, bottoms[i, j][3].Z));
            ChangeVertexValue(i, j, 3, (int)TabNames.Right, new Vertex(bottoms[i, j][0].X, tops[i, j][0].Y, bottoms[i, j][0].Z));
            ChangeVertexValue(i, j, 2, (int)TabNames.Front, new Vertex(bottoms[i, j][1].X, tops[i, j][1].Y, bottoms[i, j][1].Z));
            ChangeVertexValue(i, j, 3, (int)TabNames.Front, new Vertex(bottoms[i, j][2].X, tops[i, j][2].Y, bottoms[i, j][2].Z));
            ChangeVertexValue(i, j, 2, (int)TabNames.Back, new Vertex(bottoms[i, j][3].X, tops[i, j][3].Y, bottoms[i, j][3].Z));
            ChangeVertexValue(i, j, 3, (int)TabNames.Back, new Vertex(bottoms[i, j][2].X, tops[i, j][2].Y, bottoms[i, j][2].Z));
        }
        private void CutWithoutView()
        {
            StartEnable = false;
            for (int i = _moveCounter; i < _cutterPositions.Count; i++)
            {
                _cutterCoordNext = _cutterPositions[i];
                _cutterDiff = _cutterCoordNext - _cutterCoord;
                //if (_isCutterFlat && _cutterPositions[_moveCounter].X == _cutterPositions[_moveCounter - 1].X && _cutterPositions[_moveCounter].Z == _cutterPositions[_moveCounter - 1].Z && _cutterPositions[_moveCounter].Y < _cutterPositions[_moveCounter - 1].Y)// Math.Round((decimal)_cutterDiff.X, 2) == 0 && Math.Round((decimal)_cutterDiff.Y, 2) != 0 && Math.Round((decimal)_cutterDiff.Z, 2) == 0 && _cutterDiff.Y < 0)
                //{
                //    _simulationTimer.Stop();
                //    MessageBox.Show("Ruch pionowy płaskim frezem! Symulacja przerwana.");
                //    StartEnable = true;
                //    _moveCounter = 1;
                //    return;
                //}
                while (Math.Round((decimal)_cutterDiff.X, 2) != 0)
                {
                    var dx = Math.Abs(_cutterDiff.X);
                    if (Math.Round((decimal)_cutterDiff.X, 2) != 0)
                        _cutterCoord.X = _cutterDiff.X < 0 ? (dx < _velocity ? _cutterCoord.X - dx : _cutterCoord.X - _velocity) : (dx < _velocity ? _cutterCoord.X + dx : _cutterCoord.X + _velocity);

                    CheckDepth();
                    CheckCollisions();
                    _cutterDiff = _cutterCoordNext - _cutterCoord;
                }

                while (Math.Round((decimal)_cutterDiff.Y, 2) != 0)
                {
                    var dy = Math.Abs(_cutterDiff.Y);
                    if (Math.Round((decimal)_cutterDiff.Y, 2) != 0)
                        _cutterCoord.Y = _cutterDiff.Y < 0 ? (dy < _velocity ? _cutterCoord.Y - dy : _cutterCoord.Y - _velocity) : (dy < _velocity ? _cutterCoord.Y + dy : _cutterCoord.Y + _velocity);

                    CheckDepth();
                    CheckCollisions();
                    _cutterDiff = _cutterCoordNext - _cutterCoord;
                }

                while (Math.Round((decimal)_cutterDiff.Z, 2) != 0)
                {
                    var dz = Math.Abs(_cutterDiff.Z);
                    if (Math.Round((decimal)_cutterDiff.Z, 2) != 0)
                        _cutterCoord.Z = _cutterDiff.Z < 0 ? (dz < _velocity ? _cutterCoord.Z - dz : _cutterCoord.Z - _velocity) : (dz < _velocity ? _cutterCoord.Z + dz : _cutterCoord.Z + _velocity);

                    CheckDepth();
                    CheckCollisions();
                    _cutterDiff = _cutterCoordNext - _cutterCoord;
                }
                _moveCounter++;
                OnPropertyChanged("Move");
            }
        }
        public Vertex FindIntersections(float x0, float y0, float z0, float x1, float y1, float z1, Vertex circleCenter, double circleRadius)
        {
            double cx = circleCenter.X;
            double cy = circleCenter.Y + _radius;
            double cz = circleCenter.Z;

            double px = x0;
            double py = y0;
            double pz = z0;

            double vx = x1 - px;
            double vy = y1 - py;
            double vz = z1 - pz;

            double A = vx * vx + vy * vy + vz * vz;
            double B = 2.0 * (px * vx + py * vy + pz * vz - vx * cx - vy * cy - vz * cz);
            double C = px * px - 2 * px * cx + cx * cx + py * py - 2 * py * cy + cy * cy +
                       pz * pz - 2 * pz * cz + cz * cz - circleRadius * circleRadius;
            double D = B * B - 4 * A * C;

            if (D < 0) return new Vertex(-10, -10, -10);

            double t1 = (-B - Math.Sqrt(D)) / (2.0 * A);
            double t2 = (-B + Math.Sqrt(D)) / (2.0 * A);

            Vertex solution1 = new Vertex((float)(x0 * (1 - t1) + t1 * x1), (float)(y0 * (1 - t1) + t1 * y1), (float)(z0 * (1 - t1) + t1 * z1));
            if (D == 0) return solution1;
            Vertex solution2 = new Vertex((float)(x0 * (1 - t2) + t2 * x1), (float)(y0 * (1 - t2) + t2 * y1), (float)(z0 * (1 - t2) + t2 * z1));
            return solution1.Y < solution2.Y ? solution1 : solution2;
        }
        private void CheckDepth()
        {
            if (_cutterCoord.Y < _minY)
            {
                _simulationTimer.Stop();
                MessageBox.Show("Zbyt głębokie zanurzenie freza! Symulacja przerwana.");
                StartEnable = true;
                _moveCounter = 1;
                return;
            }
        }
        private void SimulationTimerTick(object sender, EventArgs e)
        {
            var f = _velocity;
            BuildCutterVisualization();
            _cutterDiff = _cutterCoordNext - _cutterCoord;
            if (Math.Round((decimal)_cutterDiff.X, 2) == 0 && Math.Round((decimal)_cutterDiff.Y, 2) == 0 && Math.Round((decimal)_cutterDiff.Z, 2) == 0)
            {
                if (_moveCounter == _cutterPositions.Count)
                {
                    _simulationTimer.Stop();
                    MessageBox.Show("Symulacja zakonczona.");
                    StartEnable = true;
                    _moveCounter = 1;
                }
                _cutterCoordNext = new Vertex(_cutterPositions[_moveCounter].X, _cutterPositions[_moveCounter].Y, _cutterPositions[_moveCounter].Z);
                _moveCounter++;
                OnPropertyChanged("NextPos");
                OnPropertyChanged("Move");
            }
            //else if (_isCutterFlat && Math.Round((decimal)_cutterDiff.X, 2) == 0 && Math.Round((decimal)_cutterDiff.Y, 2) != 0 && Math.Round((decimal)_cutterDiff.Z, 2) == 0 && _cutterDiff.Y < 0)
            //{
            //    _simulationTimer.Stop();
            //    MessageBox.Show("Ruch pionowy płaskim frezem! Symulacja przerwana.");
            //    StartEnable = true;
            //    _moveCounter = 1;
            //    return;
            //}
            //if (_isCutterFlat && _moveCounter < _cutterPositions.Count && _cutterPositions[_moveCounter].X == _cutterPositions[_moveCounter - 1].X && _cutterPositions[_moveCounter].Z == _cutterPositions[_moveCounter - 1].Z && _cutterPositions[_moveCounter].Y < _cutterPositions[_moveCounter - 1].Y)// Math.Round((decimal)_cutterDiff.X, 2) == 0 && Math.Round((decimal)_cutterDiff.Y, 2) != 0 && Math.Round((decimal)_cutterDiff.Z, 2) == 0 && _cutterDiff.Y < 0)
            //{
            //    _simulationTimer.Stop();
            //    MessageBox.Show("Ruch pionowy płaskim frezem! Symulacja przerwana.");
            //    StartEnable = true;
            //    _moveCounter = 1;
            //    return;
            //}
            //else
            {
                var dx = Math.Abs(_cutterDiff.X);
                var dy = Math.Abs(_cutterDiff.Y);
                var dz = Math.Abs(_cutterDiff.Z);
                if (Math.Round((decimal)_cutterDiff.X, 2) != 0)
                    _cutterCoord.X = _cutterDiff.X < 0 ? (dx < _velocity ? _cutterCoord.X - dx : _cutterCoord.X - _velocity) : (dx < _velocity ? _cutterCoord.X + dx : _cutterCoord.X + _velocity);
                if (Math.Round((decimal)_cutterDiff.Y, 2) != 0)
                    _cutterCoord.Y = _cutterDiff.Y < 0 ? (dy < _velocity ? _cutterCoord.Y - dy : _cutterCoord.Y - _velocity) : (dy < _velocity ? _cutterCoord.Y + dy : _cutterCoord.Y + _velocity);
                if (Math.Round((decimal)_cutterDiff.Z, 2) != 0)
                    _cutterCoord.Z = _cutterDiff.Z < 0 ? (dz < _velocity ? _cutterCoord.Z - dz : _cutterCoord.Z - _velocity) : (dz < _velocity ? _cutterCoord.Z + dz : _cutterCoord.Z + _velocity);
                OnPropertyChanged("Pos");
            }

            CheckDepth();
            CheckCollisions();
        }

       // FOR DEBUG
        private void DisplayModelPoints()
        {
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_TEXTURE);
            gl.PointSize(3);
            gl.Begin(OpenGL.GL_POINTS);


            gl.Color(1.0f, 0.0, 0.0f);
            foreach (var p in cloudPoints)
                gl.Vertex(p.X, p.Z, p.Y);

            if (intersections!=null)
            {
                gl.Color(0.0f, 0.0, 1.0);
                for (int i = 0; i < intersections.Count; i++)
                {
                    gl.Vertex(intersections[i].X, intersections[i].Z, intersections[i].Y);
                }

            }
            //if (writer != null)
            //    if (writer.POINTSNEXT.Count > 0)
            //    {
            //        gl.Color(0.0f, 0.0, 1.0);
            //        for (int i = 0; i < writer.POINTSNEXT.Count; i++)
            //        {
            //            gl.Vertex(writer.POINTSNEXT[i].X, writer.POINTSNEXT[i].Z, writer.POINTSNEXT[i].Y);
            //        }

            //    }


            gl.End();
            gl.Enable(OpenGL.GL_TEXTURE);
            gl.Enable(OpenGL.GL_LIGHTING);
        }
        private enum TabNames
        {
            Bottoms = 0,
            Tops = 1,
            Left = 2,
            Right = 3,
            Front = 4,
            Back = 5
        }
    }
}
