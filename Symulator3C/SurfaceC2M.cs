﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Media.Media3D;

namespace Symulator3C
{
    public class SurfaceC2 : SurfaceM
    {
        #region Private Members
        private double[] _knots;
        #endregion Private Members
        #region Public Properties
        #endregion Public Properties
        #region Constructors
        public SurfaceC2(double x, double y, double z, string name, bool isCylinder, double width, double height, int verticalPatches, int horizontalPatches, Point3D[,] points = null)
            : base(x, y, z, name, isCylinder, width, height, verticalPatches, horizontalPatches)
        {
            Continuity = 2;
            SetVertices(points, 3 + VerticalPatches, 3 + HorizontalPatches);
            SetSplineKnots();
        }
        #endregion Constructors
        #region Private Methods
        private void SetSplineKnots()
        {
            _knots = new double[2 * 4];
            _knots[0] = 0;
            for (int i = 0; i <= 5; i++)
                _knots[i + 1] = i;
            _knots[2 * 4-1] = 5;
        }
        
        public Point3D CalculatePatchValue(Point3D[,] points, double u, double v, bool uDerivative = false, bool vDerivative = false)
        {
            var point = new Point3D(0, 0, 0);

            Point3D[] tmpPoints = new Point3D[4];
            for (int t = 0; t < 4; t++)
                tmpPoints[t] = new Point3D(0, 0, 0);
            double n;

            if (!uDerivative && !vDerivative)
            {
                for (int i = 0; i < 4; i++)
                {
                    n = GetNFunctionValue(_knots, i, 3, u);
                    for (int j = 0; j < 4; j++)
                    {
                        tmpPoints[j].X += points[i, j].X * n;
                        tmpPoints[j].Y += points[i, j].Y * n;
                        tmpPoints[j].Z += points[i, j].Z * n;
                    }
                }

                for (int i = 0; i < 4; i++)
                {
                    n = GetNFunctionValue(_knots, i, 3, v);
                    point.X += tmpPoints[i].X * n;
                    point.Y += tmpPoints[i].Y * n;
                    point.Z += tmpPoints[i].Z * n;
                }

            }
            if (vDerivative)
            {
                for (int i = 0; i < 4; i++)
                {
                    n = GetNFunctionValue(_knots, i, 3, u);
                    for (int j = 0; j < 4; j++)
                    {
                        n = GetNFunctionValue(_knots, i, 3, u);
                        tmpPoints[j].X += points[i, j].X * n;
                        tmpPoints[j].Y += points[i, j].Y * n;
                        tmpPoints[j].Z += points[i, j].Z * n;
                    }
                }

                for (int i = 0; i < 3; i++)
                {
                    tmpPoints[i].X = tmpPoints[i + 1].X - tmpPoints[i].X;
                    tmpPoints[i].Y = tmpPoints[i + 1].Y - tmpPoints[i].Y;
                    tmpPoints[i].Z = tmpPoints[i + 1].Z - tmpPoints[i].Z;
                }

                for (int i = 0; i < 3; i++)
                {
                    n = GetNFunctionValue(_knots, i + 1, 2, v);
                    point.X += tmpPoints[i].X * n;
                    point.Y += tmpPoints[i].Y * n;
                    point.Z += tmpPoints[i].Z * n;
                }
            }
            if (uDerivative)
            {
                for (int i = 0; i < 4; i++)
                {
                    n = GetNFunctionValue(_knots, i, 3, v);
                    for (int j = 0; j < 4; j++)
                    {
                        tmpPoints[j].X += points[j, i].X * n;
                        tmpPoints[j].Y += points[j, i].Y * n;
                        tmpPoints[j].Z += points[j, i].Z * n;
                    }
                }

                for (int i = 0; i < 3; i++)
                {
                    tmpPoints[i].X = tmpPoints[i + 1].X - tmpPoints[i].X;
                    tmpPoints[i].Y = tmpPoints[i + 1].Y - tmpPoints[i].Y;
                    tmpPoints[i].Z = tmpPoints[i + 1].Z - tmpPoints[i].Z;
                }

                for (int i = 0; i < 3; i++)
                {
                    n = GetNFunctionValue(_knots, i + 1, 2, u);
                    point.X += tmpPoints[i].X * n;
                    point.Y += tmpPoints[i].Y * n;
                    point.Z += tmpPoints[i].Z * n;
                }
            }
            return point;
        }

        #endregion Private Methods
        #region Protected Methods
        protected override Point3D[,] GetPatchMatrix(int i, int j)
        {
            return new[,]{{Points[i + 0, j], Points[i + 0, (j + 1) % Points.GetLength(1)], Points[i + 0, (j + 2) % Points.GetLength(1)], Points[i + 0, (j + 3) % Points.GetLength(1)]}
                          , {Points[i + 1, j], Points[i + 1, (j + 1) % Points.GetLength(1)], Points[i + 1, (j + 2) % Points.GetLength(1)], Points[i + 1, (j + 3) % Points.GetLength(1)]}
                          , {Points[i + 2, j], Points[i + 2, (j + 1) % Points.GetLength(1)], Points[i + 2, (j + 2) % Points.GetLength(1)], Points[i + 2, (j + 3) % Points.GetLength(1)]}
                          , {Points[i + 3, j], Points[i + 3, (j + 1) % Points.GetLength(1)], Points[i + 3, (j + 2) % Points.GetLength(1)], Points[i + 3, (j + 3) % Points.GetLength(1)]}};
        }
        #endregion Protected Methods
        #region Public Methods

        public override Point3D CalculatePointOnSurface(int i, int j, double u, double v, bool uDerivative = false, bool vDerivative = false)
        {
            var points = GetPatchMatrix(i, j);
            return CalculatePatchValue(points, 2 + u, 2 + v, uDerivative, vDerivative);
        }

        private double GetNFunctionValue(double[] knots, int i, int n, double ti)
        {
            if (n < 0)
                return 0;
            if (n == 0)
            {
                if (ti >= knots[i] && ti < knots[i + 1])
                    return 1;
                return 0;
            }

            double n1 = GetNFunctionValue(knots, i, n - 1, ti);
            double n2 = GetNFunctionValue(knots, i + 1, n - 1, ti);
            return n1 * (ti - knots[i]) / n + n2 * (knots[i + n + 1] - ti) / n;
        }

        #endregion Public Methods
    }
}

