﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media.Media3D;
using Point = System.Windows.Point;
using Vertex = SharpGL.SceneGraph.Vertex;

namespace Symulator3C
{
    class PathWriter
    {

        private const double ExactRadius = 0.4;
        public List<List<Point3D>> interesctionGlowna, interesctionPrzednia, interesctionTylnia, interesctionCzub;
        public List<Point3D> POINTSNEXT;
        private double minX;
        private Point3DHelper _helper;
        private int _divideX, _divideY;
        private float _startX, _startZ, _deltaX, _deltaZ;
        private List<Vertex>[,] _coordTab;

        public PathWriter(List<Vertex>[,] tops, float _lenghtX, float _lenghtZ)
        {
            _helper = new Point3DHelper();
            _coordTab = tops;

            _divideX = tops.GetLength(0);
            _divideY = tops.GetLength(1);

            _startX = tops[0, 0][0].X;
            _startZ = tops[0, 0][0].Z;

            _deltaX = _lenghtX / (float)_divideX;
            _deltaZ = _lenghtZ / (float)_divideY;

            POINTSNEXT = new List<Point3D>();
        }

        #region FIRST

        public List<Vertex> CutFirstTime(float[,] HighMap, List<Point3D> cloudPoints, float radius)
        {
            float[,] NewHighMap = UpdateHighMap(HighMap, cloudPoints);

            float epsilon = 0.04f;
            List<Vertex> list = new List<Vertex>();

            float xp, zp, yp;
            for (int i = 0; i < NewHighMap.GetLength(1) - 1; i += 4)
            {
                for (int j = 0; j < NewHighMap.GetLength(0) - 1; j++)
                {
                    xp = _coordTab[j, i][0].X;
                    yp = CheckMaxHeight(NewHighMap, j, i, radius) + 1.5f * epsilon;
                    if (yp < 3.5f)
                        yp = 3.5f;
                    zp = _coordTab[j, i][0].Z;
                    list.Add(new Vertex(xp, yp, zp));
                }
                for (int j = NewHighMap.GetLength(0) - 2; j > 0; j--)
                {
                    xp = _coordTab[j, i][0].X;
                    yp = CheckMaxHeight(NewHighMap, j, i, radius) + 1.5f * epsilon;
                    zp = _coordTab[j, i][0].Z;
                    list.Add(new Vertex(xp, yp, zp));
                }
            }


            list = RemoveDuplicates(list, 0);
            list = RemoveDuplicates(list, 1);
            list = RemoveDuplicates(list, 2);
            for (int i = 0; i < list.Count; i++)
                list[i] = new Vertex(list[i].X, list[i].Y + 0.02f, list[i].Z);
            list.Add(new Vertex(list[list.Count - 1].X, 5, list[list.Count - 1].Z));
            list.Insert(0,(new Vertex(list[0].X, 5, list[0].Z)));
            return list;
        }

        private float[,] UpdateHighMap(float[,] HighMap, List<Point3D> cloudPoints)
        {
            Point tmp;
            float[,] NewHighMap = new float[HighMap.GetLength(0), HighMap.GetLength(1)];
            for (int i = 0; i < NewHighMap.GetLength(0); i++)
                for (int j = 0; j < NewHighMap.GetLength(1); j++)
                    NewHighMap[i, j] = 2;

            for (int i = 0; i < cloudPoints.Count; i++)
            {
                tmp = FindTopsCoordinates((float)cloudPoints[i].X, (float)cloudPoints[i].Y);
                if (NewHighMap[(int)tmp.X, (int)tmp.Y] > (float)cloudPoints[i].Z)
                    continue;
                NewHighMap[(int)tmp.X, (int)tmp.Y] = (float)cloudPoints[i].Z + 0.04f;
            }

            return NewHighMap;
        }

        private double FindMinX(List<Point3D> border)
        {
            double minValue = int.MaxValue;
            foreach (var b in border)
            {
                if (b.X < minValue)
                    minValue = b.X;
            }

            return minValue;
        }

        private float CheckMaxHeight(float[,] Map, int i, int j, float radius)
        {
            int horizontalDelta = (int)Math.Ceiling(radius / _deltaZ);
            int verticalDelta = (int)Math.Ceiling(radius / _deltaX);
            float maxHeight = (float)int.MinValue;

            int startA = i - verticalDelta >= 0 ? i - verticalDelta : 0;
            int startB = j - horizontalDelta >= 0 ? j - horizontalDelta : 0;

            int endA = i + verticalDelta < Map.GetLength(0) ? i + verticalDelta : Map.GetLength(0) - 1;
            int endB = j + horizontalDelta < Map.GetLength(1) ? j + horizontalDelta : Map.GetLength(1) - 1;

            for (int a = startA; a <= endA; a++)
                for (int b = startB; b <= endB; b++)
                    if (Map[a, b] > maxHeight)
                        maxHeight = Map[a, b];

            return maxHeight;
        }

        #endregion

        #region SECOND

        private void SortIntersectionsWithPlane(List<SurfaceM> surfaces)
        {
            foreach (var s in surfaces)
            {
                if (s.Name == "przednia" || s.Name == "tylnia")
                {
                    s.IntersectionUV[0] = s.IntersectionUV[0].OrderByDescending(p => p.Item1.Y).ThenBy(p => p.Item1.X).ToList();
                    s.IntersectionUV[1] = s.IntersectionUV[1].OrderBy(p => p.Item1.Y).ThenBy(p => p.Item1.X).ToList();
                }

                if (s.Name == "glowna" || s.Name == "czub" || s.Name == "wiertlo")
                {
                    s.IntersectionUV[0] = s.IntersectionUV[0].OrderByDescending(p => p.Item1.X).ToList();
                    s.IntersectionUV[1] = s.IntersectionUV[1].OrderBy(p => p.Item1.X).ToList();
                }
            }
        }

        private void AddFakeNormals(ref List<Point3D> normalVectors, int i)
        {
            for (int j = 0; j < 10; j++)
            {
                var last = normalVectors[normalVectors.Count - 1];
                var prelast = normalVectors[normalVectors.Count - 2];
                var translate = _helper.Minus(last, prelast);
                var m = _helper.Add(last, translate);
                m.Z = 2;
                if (i == 0)
                    m = _helper.Multiple(m, 1.002);
                normalVectors.Add(m);
            }
        }

        public List<Vertex> CutSecondTimeFlat(List<Point3D> border)
        {
            minX = FindMinX(border);
            List<Vertex> list = new List<Vertex>();
            var last = border[border.Count - 1];
            list.Add(new Vertex((float)last.X, 7, (float)last.Z));
            list.Add(new Vertex((float)last.X - 7, 7, (float)last.Z - 7));
            list.Add(new Vertex(-9.0f, 2f, -9.0f));
            int ratio = 1;
            int downStartIndex = _coordTab.GetLength(1) - 1, upStartIndex = 0;
            for (int i = 0; i <= _coordTab.GetLength(0); i += 2)
            {
                if (ratio > 0)
                {
                    if (i == _coordTab.GetLength(0))
                    {
                        i--;
                        for (int j = 0; j < _coordTab.GetLength(1); j += ratio)
                        {
                            list.Add(new Vertex(_coordTab[i, j][2].X, 2, _coordTab[i, j][0].Z));
                            downStartIndex = j;
                        }
                        break;
                    }
                    for (int j = 0; j < _coordTab.GetLength(1); j += ratio)
                        if (border.FindIndex(p => p.X >= _coordTab[i, j][0].X && p.X <= _coordTab[i, j][2].X && p.Y >= _coordTab[i, j][0].Z && p.Y <= _coordTab[i, j][2].Z) < 0)
                        {
                            list.Add(new Vertex(_coordTab[i, j][0].X, 2, _coordTab[i, j][0].Z));
                            downStartIndex = j;
                        }
                        else if (CheckSquare(ref list, border, i, j, ratio, 2))
                        {
                            var newStart = FindTopsCoordinates(list[list.Count - 1].X, list[list.Count - 1].Z);
                            upStartIndex = (int)newStart.Y;
                            break;
                        }
                }
                else
                {
                    for (int j = downStartIndex; j >= 0; j += ratio)
                        if (border.FindIndex(p => p.X >= _coordTab[i, j][0].X && p.X <= _coordTab[i, j][2].X && p.Y >= _coordTab[i, j][0].Z && p.Y <= _coordTab[i, j][2].Z) < 0)
                            list.Add(new Vertex(_coordTab[i, j][0].X, 2, _coordTab[i, j][0].Z));
                }
                ratio *= (-1);
            }

            ratio = -1;

            for (int i = _coordTab.GetLength(0) - 14; i > 8; i -= 2)
            {
                if (ratio < 0)
                {
                    for (int j = _coordTab.GetLength(1) - 1; j > 0; j += ratio)
                    {
                        if (border.FindIndex(p => p.X >= _coordTab[i, j][0].X && p.X <= _coordTab[i, j][2].X && p.Y >= _coordTab[i, j][0].Z && p.Y <= _coordTab[i, j][2].Z) < 0)
                        {
                            list.Add(new Vertex(_coordTab[i, j][0].X, 2, _coordTab[i, j][0].Z));
                            upStartIndex = j;
                        }
                        else if (CheckSquare(ref list, border, i, j, ratio, 2))
                        {
                            var newStart = FindTopsCoordinates(list[list.Count - 1].X, list[list.Count - 1].Z);
                            upStartIndex = (int)newStart.Y;
                            break;
                        }
                    }

                }
                else
                {
                    for (int j = upStartIndex; j < _coordTab.GetLength(1); j += ratio)
                        if (border.FindIndex(p => p.X >= _coordTab[i, j][0].X && p.X <= _coordTab[i, j][2].X && p.Y >= _coordTab[i, j][0].Z && p.Y <= _coordTab[i, j][2].Z) < 0)
                            list.Add(new Vertex(_coordTab[i, j][0].X, 2, _coordTab[i, j][0].Z));

                }
                ratio *= (-1);
            }

            list = RemoveDuplicates(list, 2);
            list = RemoveDuplicates(list, 0);
            list = RemoveDuplicates(list, 1);
            for (int i = 0; i < list.Count; i++)
                list[i] = new Vertex(list[i].X, list[i].Y + 0.02f, list[i].Z);
            list.Add(new Vertex(list[list.Count - 1].X, 5, list[list.Count - 1].Z));
            return list;
        }

        private bool CheckSquare(ref List<Vertex> list, List<Point3D> border, int i, int j, int ratio, int delta)
        {
            float epsilon = 0.025f;
            if (ratio > 0)
                for (float a = _coordTab[i, j][0].X; a < _coordTab[i, j][2].X; a += ratio * 0.01f)
                {
                    var index = border.FindIndex(p => p.Y - epsilon <= _coordTab[i, j][2].Z && p.X >= _coordTab[i, j][0].X && p.X <= _coordTab[i, j][2].X);
                    if (index >= 0)
                    {
                        list.Add(new Vertex((float)border[index].X, 2, (float)border[index].Y));
                        while (index + 1 < border.Count && border[index + 1].X < _coordTab[i + delta, j][0].X && border[index + 1].X > border[index].X)
                        {
                            index++;
                            list.Add(new Vertex((float)border[index].X, 2, (float)border[index].Y));
                        }
                        if (list[list.Count - 1].X < _coordTab[i + delta, j][0].X)
                            list.Add(new Vertex(_coordTab[i + delta, j][0].X, 2, list[list.Count - 1].Z));
                        return true;
                    }
                    else
                        list.Add(new Vertex(a, 2, _coordTab[i, j][0].Z));
                }
            else
            {
                for (float a = _coordTab[i, j][2].X; a >= _coordTab[i, j][0].X; a += ratio * 0.01f)
                {
                    var index = border.FindIndex(p => p.Y >= _coordTab[i, j][0].Z && p.X >= _coordTab[i, j][0].X && p.X <= _coordTab[i, j][2].X);
                    if (index >= 0)
                    {
                        if (border[index].X <= minX + epsilon)
                            return true;
                        while (index + 1 < border.Count && border[index + 1].X >= _coordTab[i - delta, j][0].X)
                        {
                            list.Add(new Vertex((float)border[index].X, 2, (float)border[index].Y));
                            index++;
                        }
                        return true;
                    }
                    else
                        list.Add(new Vertex(a, 2, _coordTab[i, j][2].Z));
                }
            }

            return false;
        }

        public List<Point3D> CutBorder(List<SurfaceM> surfaces, double radius)
        {
            Point3D czubNormal = new Point3D(0, 0, 0);

            bool czubFlag = true;
            SortIntersectionsWithPlane(surfaces);
            List<Point3D> border = new List<Point3D>();
            List<Tuple<Point, Point>> collisions = new List<Tuple<Point, Point>>();
            int actualIntersection = 0, prevIntersection = 0;
            int actualPoint = 0;
            int actualSurfaceIndex = 0, prevSurfacendex = 0;
            SurfaceM actualSurface = surfaces[actualSurfaceIndex];

            foreach (var s in surfaces)
            {
                s.Normals.Clear();
                for (int i = 0; i < s.IntersectionUV.Count; i++)
                {
                    var normalVectors = new List<Point3D>();
                    czubFlag = true;
                    foreach (var p in s.IntersectionUV[i])
                    {
                        if (s.Name == "czub")
                        {
                            var n = _helper.GetNormal(s, (int)p.Item2[0], (int)p.Item2[1], p.Item2[2], p.Item2[3], radius);
                            n.Z = 2;
                            if (czubFlag && normalVectors.Count > 1 && _helper.Distance(n, normalVectors[normalVectors.Count - 1]) > 1)
                            {
                                AddFakeNormals(ref normalVectors, i);
                                czubFlag = false;
                            }
                            normalVectors.Add(n);
                        }
                        else
                            normalVectors.Add(_helper.GetNormal(s, (int)p.Item2[0], (int)p.Item2[1], p.Item2[2], p.Item2[3], radius));
                    }
                    s.Normals.Add(normalVectors);
                }
            }

            czubFlag = false;
            bool flag = true;
            while (flag)
                for (int i = actualPoint; i < actualSurface.Normals[actualIntersection].Count; i++)
                {
                    if (i == actualSurface.Normals[actualIntersection].Count - 1)                  //DODAWANIE ZAKONCZEN DLA CZESCI WIERTARKI
                    {
                        var last = actualSurface.IntersectionUV[actualIntersection][i].Item1;
                        var lastNormal = border[border.Count - 1];
                        var nextFirst = actualSurface.IntersectionUV[actualIntersection == 0 ? actualIntersection + 1 : actualIntersection - 1][0].Item1;
                        var nextNormal = actualSurface.Normals[actualIntersection == 0 ? actualIntersection + 1 : actualIntersection - 1][0];

                        var line = GenerateLerp(nextFirst, last, radius);
                        var arc = GenerateSlerp(lastNormal, line[0], last, radius);

                        foreach (var p in arc)
                            border.Add(p);

                        foreach (var p in line)
                            border.Add(p);
                        lastNormal = border[border.Count - 1];
                        arc = GenerateSlerp(line[line.Count - 1], nextNormal, nextFirst, radius);

                        foreach (var p in arc)
                            border.Add(p);

                        if (actualSurface.Name == "glowna")
                        {
                            flag = false;
                            break;
                        }
                        else
                        {
                            actualIntersection = actualIntersection == 0 ? actualIntersection + 1 : actualIntersection - 1;
                            actualPoint = 0;

                            if (actualSurface.Name == "wiertlo")
                                czubFlag = true;
                            break;
                        }
                    }
                    var actualNormal = actualSurface.Normals[actualIntersection][i];
                    actualNormal.Z = 2;
                    Tuple<int, int, int, Point3D> newPoint;
                    if (czubFlag || (i == actualSurface.Normals[actualIntersection].Count - 2 && actualSurface.Name == "czub"))
                        newPoint = CheckNeighborhood(surfaces, actualSurface, actualNormal, true);
                    else
                        newPoint = CheckNeighborhood(surfaces, actualSurface, actualNormal);
                    if (newPoint.Item1 < 0 || collisions.FindIndex(a => a.Item1.X == newPoint.Item2 && a.Item1.Y == newPoint.Item1 && a.Item2.X == actualIntersection && a.Item2.Y == actualSurfaceIndex) >= 0)
                    {
                        if (border.Count > 0 && _helper.Distance(border[border.Count - 1], actualNormal) > 0.4)
                        {
                            var line = GenerateLerpSimple(border[border.Count - 1], actualNormal);
                            for (int l = 1; l < line.Count; l++)
                                border.Add(line[l]);
                        }
                        else
                            border.Add(actualNormal);
                    }
                    else
                    {
                        actualSurface = surfaces[newPoint.Item1];
                        prevIntersection = actualIntersection;
                        actualIntersection = newPoint.Item2;
                        actualPoint = newPoint.Item3;
                        border.Add(newPoint.Item4);
                        prevSurfacendex = actualSurfaceIndex;
                        actualSurfaceIndex = newPoint.Item1;

                        collisions.Add(new Tuple<Point, Point>(new Point(prevIntersection, prevSurfacendex), new Point(actualIntersection, actualSurfaceIndex)));
                        if (czubFlag)
                            czubFlag = false;
                        break;
                    }
                }

            border.Insert(0, new Point3D(9, border[0].Y, border[0].Z));
            border.Insert(0, new Point3D(9, border[0].Y, 5));

            //for (int i = 0; i < border.Count; i++)
            //    border[i] = new Point3D(border[i].X, border[i].Y + 0.02, border[i].Z);
            return border;
        }

        private Tuple<int, int, int, Point3D> CheckNeighborhood(List<SurfaceM> surfaces, SurfaceM actual, Point3D normal, bool isCzub = false)
        {
            for (int s = 0; s < surfaces.Count; s++)
                for (int i = 0; i < surfaces[s].IntersectionWith.Count; i++)
                {
                    if (surfaces[s].IntersectionWith[i] != null || surfaces[s] == actual) continue;

                    for (int j = 0; j < surfaces[s].Normals.Count; j++)
                        for (int k = 0; k < surfaces[s].Normals[j].Count; k++)
                        {
                            var normalVector = surfaces[s].Normals[j][k];
                            normalVector.Z = 2;

                            if (isCzub && Math.Abs(normalVector.X - normal.X) < 0.02 && Math.Abs(normalVector.Y - normal.Y) < 0.2)
                                return new Tuple<int, int, int, Point3D>(s, j, k, normalVector);
                            if (Math.Abs(normalVector.X - normal.X) < 0.05 && Math.Abs(normalVector.Y - normal.Y) < 0.06)
                                return new Tuple<int, int, int, Point3D>(s, j, k, normalVector);
                        }
                }
            return new Tuple<int, int, int, Point3D>(-1, -1, -1, new Point3D(-1, -1, -1));
        }

        private List<Point3D> GenerateLerpSimple(Point3D end, Point3D start)
        {
            List<Point3D> line = new List<Point3D>();
            double t = 0.05;
            for (int i = 0; i < 20; i++)
            {
                var a = _helper.Multiple(start, t * i);
                var b = _helper.Multiple(end, 1 - (t * i));
                line.Add(_helper.Add(a, b));
            }
            return line;
        }

        private List<Point3D> GenerateLerp(Point3D start, Point3D end, double radius)
        {
            //  float epsilon = 0.02f;
            double t = 0.05;
            List<Point3D> line = new List<Point3D>();
            Point3D between = _helper.Minus(end, start);
            Point perpendicular = new Point((-1) * between.Y, between.X);

            perpendicular = _helper.Multiple2(_helper.Normalize2(perpendicular), radius);

            for (int i = 0; i < 20; i++)
            {
                var a = _helper.Multiple(start, t * i);
                var b = _helper.Multiple(end, 1 - (t * i));
                var newPoint = _helper.Add(a, b);

                var another = new Point3D(newPoint.X + perpendicular.X, newPoint.Y + perpendicular.Y, 2);
                line.Add(another);
            }
            return line;
        }

        private List<Point3D> GenerateSlerp(Point3D start, Point3D end, Point3D center, double radius)
        {
            List<Point3D> arc = new List<Point3D>();
            float epsilon = 0.02f;
            double t = 0.05;
            Point start2 = new Point(start.X - center.X, start.Y - center.Y);
            Point end2 = new Point(end.X - center.X, end.Y - center.Y);
            start2 = _helper.Normalize2(start2);
            end2 = _helper.Normalize2(end2);
            double angle = Math.Acos(_helper.DotProduct2(start2, end2) / (_helper.Length2(start2) * _helper.Length2(end2)));

            for (int i = 0; i < 20; i++)
            {
                var a = Math.Sin((1 - t * i) * angle) / Math.Sin(angle);
                var b = _helper.Multiple2(start2, a);

                var c = Math.Sin((t * i) * angle) / Math.Sin(angle);
                var d = _helper.Multiple2(end2, c);

                var newPoint = new Point(b.X + d.X, b.Y + d.Y);
                newPoint = _helper.Multiple2(newPoint, radius);// + epsilon);
                arc.Add(new Point3D(newPoint.X + center.X, newPoint.Y + center.Y, 2));
            }

            return arc;
        }

        private List<Vertex> RemoveDuplicates(List<Vertex> l, int direction)
        {
            List<Vertex> list = new List<Vertex>(l);
            int first = 0, last = 0;
            for (int i = 1; i < list.Count; i++)
            {
                var actual = list[i];
                var fir = list[first];
                var la = list[last];
                if (direction == 0 && list[i].X != list[first].X && list[i].Y == list[first].Y && list[i].Z == list[first].Z)   //LEWA-PRAWA
                    last++;
                else if (direction == 1 && list[i].X == list[first].X && list[i].Y != list[first].Y && list[i].Z == list[first].Z)  //W PIONIE
                    last++;
                else if (direction == 2 && list[i].X == list[first].X && list[i].Y == list[first].Y && list[i].Z != list[first].Z)  //GORA-DOL
                    last++;
                else if (i - first > 1)
                {
                    list.RemoveRange(first + 1, last - 1);
                    first += 2;
                    i = first;
                    last = 0;
                }
                else
                    first = i;
            }
            return list;
        }

        private Point FindTopsCoordinates(float x, float z)
        {
            int i = -1, j = -1;

            double xF = ((x - _startX) / _deltaX);
            i = xF % 10 == 0 ? (int)xF + 1 : (int)xF + 2;
            if (i >= _divideX)
                i = _divideX - 1;

            double ZF = ((z - _startZ) / _deltaZ);
            j = ZF % 10 == 0 ? (int)ZF + 1 : (int)ZF + 2;
            if (j >= _divideY)
                j = _divideY - 1;

            return new Point(i, j);
        }

        #endregion

        #region THIRD

        public List<Vertex> CutThirdTime(List<SurfaceM> surfaces, double radius)
        {
            foreach (var s in surfaces)
            {
                s.Normals.Clear();
                foreach (var intersection in s.IntersectionUV)
                {
                    var normalVectors = new List<Point3D>();
                    foreach (var p in intersection)
                        normalVectors.Add(_helper.GetNormal(s, (int)p.Item2[0], (int)p.Item2[1], p.Item2[2], p.Item2[3], radius));

                    s.Normals.Add(normalVectors);
                }
            }

            MakeIntersectionsPoints3D(surfaces[0]);

            var list = new List<Vertex>();

            CutTheSurface(surfaces[0], radius, 0.125, ref list);
            var last = list[list.Count - 1];
            list.Add(new Vertex((float)last.X, 5, (float)last.Z));
            CutTheSurface(surfaces[1], radius, 0.0625, ref list);
            last = list[list.Count - 1];
            list.Add(new Vertex((float)last.X, 5, (float)last.Z));
            CutTheSurface(surfaces[4], radius, 0.1, ref list);
            last = list[list.Count - 1];
            list.Add(new Vertex((float)last.X, 5, (float)last.Z));
            CutTheSurface(surfaces[3], radius, 0.1, ref list);
            last = list[list.Count - 1];
            list.Add(new Vertex((float)last.X, 5, (float)last.Z));
            CutTheSurface(surfaces[2], radius, 0.125, ref list);
            last = list[list.Count - 1];
            list.Add(new Vertex((float)last.X, 5, (float)last.Z));
            list.Add(new Vertex(9, list[list.Count - 1].Y, list[list.Count - 1].Z));


            list = RemoveDuplicates(list, 0);
            list = RemoveDuplicates(list, 1);
            list = RemoveDuplicates(list, 2);
            for (int i = 0; i < list.Count; i++)
                list[i] = new Vertex(list[i].X, list[i].Y + 0.02f, list[i].Z);
            return list;
        }

        private void MakeIntersectionsPoints3D(SurfaceM s)
        {
            interesctionGlowna = new List<List<Point3D>>();
            interesctionPrzednia = new List<List<Point3D>>();
            interesctionTylnia = new List<List<Point3D>>();
            interesctionCzub = new List<List<Point3D>>();

            List<Point3D> tmp;
            List<double[]> normalsOnSurface;
            for (int i = 0; i < s.IntersectionWith.Count; i++)
            {
                normalsOnSurface = new List<double[]>();
                tmp = new List<Point3D>();
                if (s.IntersectionWith[i] != null)
                {
                    var norms = s.IntersectionWith[i].Normals[s.IntersectionWith[i].IntersectionWith.FindIndex(a => a == s)];

                    foreach (var n in norms)
                        tmp.Add(new Point3D(n.X, n.Y, n.Z - ExactRadius + 0.02));
                }

                if (s.Name == "glowna")
                    interesctionGlowna.Add(tmp);
                if (s.IntersectionWith[i] != null && s.IntersectionWith[i].Name == "przednia")
                    interesctionPrzednia.Add(tmp);
                if (s.IntersectionWith[i] != null && s.IntersectionWith[i].Name == "tylnia")
                    interesctionTylnia.Add(tmp);
                if (s.IntersectionWith[i] != null && s.IntersectionWith[i].Name == "czub")
                    interesctionCzub.Add(tmp);
            }
        }

        private Point CheckBoundingJ(SurfaceM s)
        {
            if (s.Name == "czub")
                return new Point(s.VerticalPatches - 1, 0);
            double startV = 0;
            int end = -1, start = -1;
            Point3D tmp;
            bool flag = true, flagReverse = false;
            for (int j = 0; j < s.HorizontalPatches && flag; j++)
                for (double v = 0; v < 1; v += 0.05)
                {
                    tmp = s.CalculatePointOnSurface(0, j, 0, v);
                    if (tmp.Z >= 2 && j == 0)
                    {
                        flagReverse = true;
                        flag = false;
                        break;
                    }
                    else if (tmp.Z >= 2)
                    {
                        start = j;
                        startV = v;
                        flag = false;
                        break;
                    }
                }

            flag = true;
            if (flagReverse)
            {
                for (int j = 0; j > (-1) * s.HorizontalPatches && flag; j--)
                    for (double v = 1; v >= 0; v -= 0.05)
                    {
                        var jj = j < 0 ? s.HorizontalPatches + j : j;
                        tmp = s.CalculatePointOnSurface(0, jj, 0, v);
                        if (tmp.Z < 2)
                        {
                            start = jj;
                            startV = v + 0.1;
                            flag = false;
                            break;
                        }

                    }
            }
            flag = true;
            if (flagReverse)
            {
                start += 1;
                startV = 0.1;
            }
            for (int j = start; (j < start + s.HorizontalPatches) && flag; j++)
                for (double v = startV; v < 1; v += 0.05)
                {

                    var jj = j % s.HorizontalPatches;
                    tmp = s.CalculatePointOnSurface(0, jj, 0, v);
                    if (tmp.Z <= 2)
                    {
                        end = j % s.HorizontalPatches;
                        flag = false;
                        break;
                    }
                    startV = 0;
                }

            return new Point(start, end);
        }

        private void CutTheSurface(SurfaceM p, double radius, double delta, ref List<Vertex> list)
        {
            bool isHand = (p.Name == "przednia" || p.Name == "tylnia") ? true : false;
            bool isCzub = (p.Name == "czub") ? true : false;
            bool isWiertlo = (p.Name == "wiertlo") ? true : false;

            int counter = list.Count;
            int ratio = 1;
            double vDelta = 0.05;

            bool flag = true, flagChanged = false, flag2 = true, flag3 = true, flagReverse = false;
            Point boundJ = CheckBoundingJ(p);
            Point3D controlPoint = new Point3D(-1, -1, -1), handCheck = new Point3D(-1, -1, -1);
            int lastChange = -1;
            if (boundJ.X >= boundJ.Y)
                flagReverse = true;

            double[] newStart = new double[] { p.VerticalPatches - 1, boundJ.X, 1, 0 };
            bool ifRet = true;
            if (flagReverse)
            {
                newStart = new double[] { p.VerticalPatches - 1, boundJ.X, 1, 0 };
                var tmp = boundJ.Y;
                boundJ.Y = boundJ.X;
                boundJ.X = tmp;
                for (int i = (int)newStart[0]; (isWiertlo ? i >= 1 : i >= 0) && flag3; i--)
                {
                    for (double u = newStart[2]; (isWiertlo ? (i == 1 ? u >= 0.5 : u >= 0) : u >= 0) && flag2; u -= delta)
                    {
                        flag = true;
                        for (int jj = (int)newStart[1]; (ratio > 0 ? jj >= (int)boundJ.X : jj <= (int)boundJ.Y) && flag; jj += ratio)
                        {
                            var j = ratio > 0 ? jj % p.HorizontalPatches : jj < 0 ? p.HorizontalPatches + jj : jj;
                            if (jj != j && ratio > 0 && j > (int)boundJ.X) break;
                            if (jj != j && ratio < 0 && j < (int)boundJ.Y) break;

                            for (double v = newStart[3]; (ratio > 0 ? v < 1 : v >= 0); v += ratio * 0.05)
                            {
                                if (_helper.GetNormal(p, i, j, u, v, radius).Z - radius < 2)
                                    continue;
                                var vertex = _helper.GetNormal(p, i, j, u, v, radius);
                                vertex.Z -= ExactRadius;
                                if (isCzub)
                                {
                                    var ifreturn = CheckNormalsCollisions(p, vertex, new Point3D(0, 0, 0));
                                    if (ifreturn.Item1)
                                    {
                                        flag = false;
                                        flagChanged = true;
                                        break;
                                    }
                                }
                                var ind = CheckInteresctionSquare(p, i, j);
                                if (ind >= 0 && ratio > 0)
                                {
                                    var next = FindNextPoint(p, i, j, u, v, 0.1);
                                    var ifreturn = CheckNormalsCollisions(p, vertex, next);
                                    if (list.Count == counter)
                                        list.Add(new Vertex((float)vertex.X, 5, (float)vertex.Y));
                                    list.Add(new Vertex((float)vertex.X, (float)vertex.Z, (float)vertex.Y));

                                    flagChanged = false;
                                    if (ifreturn.Item1)
                                    {
                                        flag = false;
                                        if (p.IntersectionDirection[ind] == 1)
                                        {
                                            flag2 = false;
                                            flag3 = false;
                                        }
                                        break;
                                    }
                                }
                                else
                                {
                                    if (list.Count == counter)
                                        list.Add(new Vertex((float)vertex.X, 5, (float)vertex.Y));
                                    list.Add(new Vertex((float)vertex.X, (float)vertex.Z, (float)vertex.Y));
                                    flagChanged = false;
                                }

                                if (flag)
                                    newStart[3] = ratio > 0 ? 0 : 1;
                            }

                        }

                        if (flagChanged == false)
                        {
                            newStart[0] = i - 1;
                            newStart[1] = ratio > 0 ? boundJ.X : boundJ.Y;
                            newStart[2] = 0.9;
                            newStart[3] = ratio > 0 ? 1 : 0;
                        }
                        ratio *= (-1);
                    }
                    if (p.Name == "czub")
                    {
                        var last = list[list.Count - 1];
                        list.Add(new Vertex((float)last.X, (float)last.Y + 3, (float)last.Z));
                        counter = list.Count;
                    }
                }
            }
            else
                for (int i = (int)newStart[0]; i >= 0 && flag3; i--)
                    for (double u = newStart[2]; u >= 0 && flag2; u -= delta)
                    {
                        flag = true;
                        for (int j = (int)newStart[1]; (ratio > 0 ? j <= (int)boundJ.Y : j >= (int)boundJ.X) && flag; j += ratio)
                            for (double v = newStart[3]; (ratio > 0 ? v < 1 : v >= 0); v += ratio * vDelta)
                            {
                                if (_helper.GetNormal(p, i, j, u, v, radius).Z - radius < 2)
                                    continue;
                                var vertex = _helper.GetNormal(p, i, j, u, v, radius);
                                vertex.Z -= ExactRadius;

                                if (controlPoint.Z != -1 && (Math.Abs(controlPoint.X - vertex.X) > 0.055 && Math.Abs(controlPoint.Y - vertex.Y) < 0.1 || Math.Abs(controlPoint.Z - vertex.Z) > 0.03))
                                {
                                    //POMIJANIE PUNKTOW GDY WRACAMY OD DOLU I NIE NAPOTKALISMY INTERSEKCJI
                                    continue;
                                }

                                else
                                {
                                    //DODANIE PIERWSZEGO PUNKTU GDY WRACAMY, BO NAPOTKALISMY KRZYWA INTERSEKCJI
                                    controlPoint = new Point3D(-1, -1, -1);
                                    if (lastChange == 4)// && i >= 6)
                                    {
                                        list[list.Count - 4] = new Vertex(list[list.Count - 4].X, (float)vertex.Z, (float)vertex.Y);
                                        list[list.Count - 3] = new Vertex(list[list.Count - 3].X, (float)vertex.Z, (float)vertex.Y);
                                        list[list.Count - 2] = new Vertex(list[list.Count - 2].X, (float)vertex.Z, (float)vertex.Y);
                                        list[list.Count - 1] = new Vertex(list[list.Count - 1].X, (float)vertex.Z, (float)vertex.Y);
                                    }
                                    else if (lastChange == 3 && i >= 2)
                                    {
                                        list[list.Count - 3] = new Vertex(list[list.Count - 3].X, (float)vertex.Z, (float)vertex.Y);
                                        list[list.Count - 2] = new Vertex(list[list.Count - 2].X, (float)vertex.Z, (float)vertex.Y);
                                        list[list.Count - 1] = new Vertex(list[list.Count - 1].X, (float)vertex.Z, (float)vertex.Y);
                                    }
                                    lastChange = -1;
                                }


                                var ind = CheckInteresctionSquare(p, i, j);
                                if (ind >= 0 && ratio > 0)
                                {
                                    var next = FindNextPoint(p, i, j, u, v, 0.1);
                                    var ifreturn = CheckNormalsCollisions(p, vertex, next);
                                    ifRet = ifreturn.Item1;

                                    if (list.Count == counter)
                                        list.Add(new Vertex((float)vertex.X, 5, (float)vertex.Y));
                                    if (isHand == false || (isHand && ifreturn.Item1 == false))
                                        list.Add(new Vertex((float)vertex.X, (float)vertex.Z, (float)vertex.Y));
                                    flagChanged = false;
                                    if (ifreturn.Item1)
                                    {
                                        lastChange = ifreturn.Item2;
                                        controlPoint = ifreturn.Item3;
                                        newStart[2] += ratio;
                                        newStart[3] += ratio * 0.05;
                                        flag = false;
                                        if (p.IntersectionDirection[ind] == 1)
                                        {
                                            flag2 = false;
                                            flag3 = false;
                                        }
                                        break;
                                    }
                                }
                                else
                                {

                                    if (list.Count == counter)
                                        list.Add(new Vertex((float)vertex.X, 5, (float)vertex.Y));
                                    list.Add(new Vertex((float)vertex.X, (float)vertex.Z, (float)vertex.Y));
                                    flagChanged = false;
                                }

                                if (flag)
                                    newStart[3] = ratio > 0 ? 0 : 1;
                            }

                        if (flagChanged == false)
                        {
                            newStart[0] = i - 1;
                            newStart[1] = ratio > 0 ? boundJ.Y : boundJ.X;
                            newStart[2] = 0.9;
                            newStart[3] = ratio > 0 ? 1 : 0;
                        }
                        ratio *= (-1);
                        ifRet = !ifRet;
                    }
            if (p.Name == "tylnia")
                list.Remove(list[list.Count - 1]);

        }

        private List<double[]> DistinctList(List<double[]> list)
        {
            List<double[]> readyList = new List<double[]>(list);

            for (int i = readyList.Count - 1; i > 1; i--)
            {
                var current = readyList[i];
                var previous = readyList[i - 1];
                if (current[0] == previous[0])
                    if (current[1] == previous[1])
                        if (current[2] == previous[2] && current[3] == previous[3])
                            readyList.RemoveAt(i);
            }
            return readyList;
        }

        private Tuple<bool, int, Point3D> CheckNormalsCollisions(SurfaceM s, Point3D b, Point3D next)
        {
            var checkList = s.Name == "glowna" ? interesctionGlowna : s.Name == "przednia" ? interesctionPrzednia : s.Name == "tylnia" ? interesctionTylnia : s.Name == "czub" ? interesctionCzub : null;

            if (checkList == null)
                return new Tuple<bool, int, Point3D>(false, -1, new Point3D(-1, -1, -1));

            for (int i = 0; i < checkList.Count; i++)
                for (int j = 0; j < checkList[i].Count; j++)
                {
                    if (i == 4 && s.IntersectionDirection[i] == 2 && Math.Abs(checkList[i][j].X - b.X) < 0.05 && Math.Abs(checkList[i][j].Z - b.Z) < 0.23)
                    {
                        while (checkList[i][j].X > next.X)
                        {
                            var ch = checkList[i][j];
                            j--;
                        }
                        return new Tuple<bool, int, Point3D>(true, i, new Point3D(checkList[i][j].X, 0, checkList[i][j].Z));
                    }

                    if (i == 3 && s.IntersectionDirection[i] == 2 && Math.Abs(checkList[i][j].X - b.X) < 0.05 && Math.Abs(checkList[i][j].Z - b.Z) < 0.05)
                    {
                        while (checkList[i][j].X > next.X)
                        {
                            var ch = checkList[i][j];
                            j--;
                        }
                        return new Tuple<bool, int, Point3D>(true, i, new Point3D(checkList[i][j].X, 0, checkList[i][j].Z));
                    }
                    if (s.Name == "glowna" && s.IntersectionDirection[i] == 1 && Math.Abs(checkList[i][j].X - b.X) < 0.05 && Math.Abs(checkList[i][j].Z - b.Z) < 0.05)
                        return new Tuple<bool, int, Point3D>(true, i, new Point3D(checkList[i][j].X, 0, checkList[i][j].Z));
                    else if ((s.Name == "przednia" || s.Name == "tylnia") && Math.Abs(checkList[i][j].Y + (ExactRadius / 2) - b.Y) < 0.03)
                        return new Tuple<bool, int, Point3D>(true, i, new Point3D(checkList[i][j].X, 0, checkList[i][j].Z));
                    else if (s.Name == "czub" && checkList[i][j].X < b.X)
                        return new Tuple<bool, int, Point3D>(true, i, new Point3D(checkList[i][j].X, 0, checkList[i][j].Z));
                }

            return new Tuple<bool, int, Point3D>(false, -1, new Point3D(-1, -1, -1));
        }

        private int CheckInteresctionSquare(SurfaceM s, int i, int j)
        {
            for (int a = 0; a < s.IntersectionUV.Count; a++)
            {
                if (s.IntersectionWith[a] == null)
                    continue;
                else
                {

                    //  var start = s.Name == "glowna" ? s.AnotherNormalsOnSurface[a].FindIndex(p => (int)p[0] == i && (int)p[1] == j)
                    var start = s.IntersectionUV[a].FindIndex(p => (int)p.Item2[0] == i && (int)p.Item2[1] == j);
                    if (start < 0)
                        continue;
                    return a;
                }
            }
            return -1;
        }

        private Point3D FindNextPoint(SurfaceM s, int i, int j, double u, double v, double delta)
        {
            var newI = i;
            var newU = u - delta;
            if (newU < 0 && i > 0)
            {
                newI = i - 1;
                newU += 1;
            }
            else if (i == 0)
                newU = 0.0;

            return s.CalculatePointOnSurface(newI, j, newU, v);
        }

        #endregion

        //private double[] CheckNoIntersections(SurfaceM s, int interesectionIndex, int i, int j, double u, double v, double delta, double radius, int ratio, ref List<Vertex> list)
        //{
        //    //if (s.IntersectionDirection[interesectionIndex] == 1)
        //    //{
        //    //    CheckRestIntersection(s, interesectionIndex, i, j, u, v, delta, radius, ratio, ref list);
        //    //    return new double[] { -1 };
        //    //}

        //    double epsilon = 0.015;
        //    double epsilonV = 0.025;
        //    var nextPointX = FindNextPoint(s, i, j, u, v, delta).X;
        //    var nextPointDoubles = FindNextPointDouble(s, i, j, u, v, delta);
        //    var pointOnSurface = s.CalculatePointOnSurface(i, j, u, v);
        //    //   pointOnSurface.Z -= ExactRadius;
        //    var pointOnSurfaceNormal = _helper.GetNormal(s, i, j, u, v, radius);
        //    pointOnSurfaceNormal.Z -= ExactRadius;

        //    //  var norms = s.IntersectionWith[interesectionIndex].Normals[s.IntersectionWith[interesectionIndex].IntersectionWith.FindIndex(a => a == s)];
        //    //   var norms = s.AnotherNormalsOnSurface[interesectionIndex];
        //    List<double[]> norms;
        //    if (s.Name == "glowna" && (interesectionIndex == 3 || interesectionIndex == 4))
        //        norms = new List<double[]>(s.AnotherNormalsOnSurface[interesectionIndex]);
        //    else
        //    {
        //        norms = new List<double[]>();
        //        foreach (var inters in s.IntersectionUV[interesectionIndex])
        //            norms.Add(inters.Item2);
        //    }




        //    norms = DistinctList(norms);
        //    double minX = int.MaxValue;
        //    foreach (var n in norms)
        //    {
        //        if ((int)n[0] < 0)
        //            n[0] = 0;
        //        var p = s.CalculatePointOnSurface((int)n[0], (int)n[1], n[2], n[3]);
        //        if (p.X < minX)
        //            minX = p.X;
        //    }
        //    if (minX > nextPointX)
        //        nextPointX = minX;

        //    for (int a = 0; a < norms.Count; a++)
        //    {
        //        var tmp = s.CalculatePointOnSurface((int)norms[a][0], (int)norms[a][1], norms[a][2], norms[a][3]);
        //        //  var tmpN = _helper.GetNormal(s, (int)norms[a][0], (int)norms[a][1], norms[a][2], norms[a][3], radius);

        //        // tmp.Z += radius + 0.02;

        //        //  POINTSNEXT.Add(tmpN);
        //        if (Math.Abs(tmp.X - pointOnSurface.X) < epsilonV && Math.Abs(tmp.Y - pointOnSurface.Y) < epsilon && Math.Abs(tmp.Z - pointOnSurface.Z) < epsilon)
        //        {//POINTSNEXT.Add(tmp);
        //         // var actualP = tmp;
        //         //  tmpN = _helper.GetNormal(s, (int)norms[a][0], (int)norms[a][1], norms[a][2], norms[a][3], radius);

        //            //   POINTSNEXT.Add(tmp);
        //            list.Add(new Vertex((float)tmp.X, (float)tmp.Z, (float)tmp.Y));
        //            //if (tmpN.Z - radius <= 2)
        //            //    list.Add(new Vertex((float)tmpN.X, 2, (float)tmpN.Y));
        //            //else
        //            // list.Add(new Vertex((float)tmpN.X, (float)tmpN.Z, (float)tmpN.Y));
        //            while (tmp.X > nextPointX && a < norms.Count && tmp.Z > 2)
        //            {
        //                // if (tmp.X == nextPointX)
        //                //   break;
        //                //   list.Add(new Vertex((float)tmpN.X, (float)tmpN.Z, (float)tmpN.Y));
        //                a--;
        //                if (a < 0)
        //                    a = norms.Count - 1;
        //                tmp = s.CalculatePointOnSurface((int)norms[a][0], (int)norms[a][1], norms[a][2], norms[a][3]);
        //            }

        //            //    tmpN = _helper.GetNormal(s, (int)norms[a][0], (int)norms[a][1], norms[a][2], norms[a][3], radius);

        //            //if (tmpN.Z - radius <= 2)
        //            //    list.Add(new Vertex((float)tmpN.X, 2, (float)tmpN.Y));
        //            //else
        //            POINTSNEXT.Add(tmp);
        //            list.Add(new Vertex((float)tmp.X, (float)tmp.Z, (float)tmp.Y));

        //            tmp.Z += ExactRadius;
        //            //    var next = FindNextPointDouble(s, i, j, u, v, delta);
        //            var array = FindCoordinatesUV(s, i, j, 0.05, 0.05, tmp);
        //            return array;
        //            //       POINTSNEXT.Add(s.CalculatePointOnSurface((int)next[0],(int)next[1],next[2],next[3]));
        //            // return FindNextPointDouble(s, i, j, u, v, delta);
        //        }

        //    }

        //    return new double[] { -1 };
        //}
    }
}
