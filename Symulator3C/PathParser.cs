﻿using System.Collections.Generic;
using SharpGL.SceneGraph;
using Microsoft.Win32;
using System.IO;
using System.Text;

namespace Symulator3C
{
    class PathParser
    {
        private bool _isFlat, _isOk;
        private int _cutterRadius;
        private StreamReader _streamReader;
        private string _fileName;
        private List<Vertex> _cutterPositions;
        public List<Vertex> CutterPositions
        {
            get { return _cutterPositions; }
        }
        public string FileName
        {
            get { return _fileName; }
        }
        public bool IsCutterFlat
        {
            get { return _isFlat; }
        }
        public int CutterRadius
        {
            get { return _cutterRadius; }
        }
        public bool IsParserOk
        {
            get { return _isOk; }
        }

        public void InitReader()
        {
            var fileDialog = new OpenFileDialog();
            fileDialog.Filter = @"Path files (*.k??;*.f??)|*.k??;*.f??";
            if (fileDialog.ShowDialog() != true)
            {
                _isOk = false;
                return;
            }
            _isOk = true;
            _fileName = Path.GetFileName(fileDialog.FileName);
            string type = Path.GetExtension(fileDialog.FileName);
            _isFlat = type.Contains("f") ? true : false;
            _cutterRadius = int.Parse(type.Substring(2));
            _streamReader = new StreamReader(fileDialog.FileName);
            _cutterPositions = new List<Vertex>();
        }
        public void Read()
        {
            string line;
            while ((line = _streamReader.ReadLine()) != null)
            {
                int xIndex = line.IndexOf("X", System.StringComparison.Ordinal) + 1;
                int yIndex = line.IndexOf("Y", System.StringComparison.Ordinal) + 1;
                int zIndex = line.IndexOf("Z", System.StringComparison.Ordinal) + 1;
                _cutterPositions.Add(new Vertex(float.Parse(line.Substring(xIndex, yIndex - xIndex - 1)) * 0.1f, float.Parse(line.Substring(zIndex, line.Length - zIndex)) * 0.1f, float.Parse(line.Substring(yIndex, zIndex - yIndex - 1)) * -0.1f));
            }

            _streamReader.Close();
        }

        public void Write(List<Vertex> points, string name)
        {
            var stringBuilder = new StringBuilder();
            int counter = 0;
            string line;
            foreach (var p in points)
            {
                line = "N" + counter.ToString() + "G01" + "X" + (p.X * 10.00000f).ToString() + "Y" + (p.Z * -10.00000f).ToString() + "Z" + (p.Y * 10.00000f).ToString();
                stringBuilder.AppendLine(line);
                counter++;
            }
            
            stringBuilder = stringBuilder.Replace(',', '.');
            using (var streamWriter = new StreamWriter(name))
                streamWriter.Write(stringBuilder.ToString());
        }
    }
}
