﻿using System;
using System.Collections.Generic;
using System.Windows.Media.Media3D;

namespace Symulator3C
{
    public abstract class SurfaceM
    {

        #region Protected Properties

        protected double Width { get; private set; }
        protected double Height { get; private set; }
        protected int Continuity { get; set; }
        public List<List<Tuple<Point3D, double[]>>> IntersectionUV { get; set; }
        public List<SurfaceM> IntersectionWith { get; set; }
        public List<int> IntersectionDirection { get; set; }
        public List<List<Point3D>> Normals { get; set; }
        public List<List<double[]>> AnotherNormalsOnSurface { get; set; }
        public string Name { get; private set; }
        public List<Point3D> SurfacePoints { get; protected set; }
        public List<int[]> MappingPoint { get; protected set; }
        double xx, yy, zz;
        #endregion Protected Properties
        #region Public Properties
        public Point3D[,] Points;
        public bool IsCylinder { get; private set; }
        public int HorizontalPatches { get; private set; }
        public int VerticalPatches { get; private set; }
        #endregion Public Properties
        #region Constructor
        protected SurfaceM(double x, double y, double z, string name, bool isCylinder, double width, double height, int verticalPatches, int horizontalPatches)
        {
            VerticalPatches = verticalPatches;
            HorizontalPatches = horizontalPatches;
            IsCylinder = isCylinder;
            Width = width;
            Height = height;
            xx = x;
            yy = y;
            zz = z;
            Name = name;
            IntersectionUV = new List<List<Tuple<Point3D, double[]>>>();
            Normals = new List<List<Point3D>>();
            AnotherNormalsOnSurface = new List<List<double[]>>();
            IntersectionWith = new List<SurfaceM>();
            SetDirections();
        }
        #endregion Constructors
        #region Private Methods
        private void SetCylinderVertices()
        {
            double topLeftY = 0;
            double radius = Width;
            double alpha = (Math.PI * 2.0f) / Points.GetLength(1);
            double dy = Height / Points.GetLength(0);

            for (int i = 0; i < Points.GetLength(0); i++)
                for (int j = 0; j < Points.GetLength(1); j++)
                {
                    var point = new Point3D(radius * Math.Cos(alpha * j), topLeftY + (i * dy), radius * Math.Sin(alpha * j));
                    Points[i, j] = point;
                }
        }
        private void SetPlaneVertices()
        {
            Vector4 topLeft = new Vector4(xx, yy, zz, 1);
            double dx = Width / (HorizontalPatches * 3);
            double dy = Height / (VerticalPatches * 3);

            for (int i = 0; i < Points.GetLength(0); i++)
                for (int j = 0; j < Points.GetLength(1); j++)
                {
                    var point = new Point3D(topLeft.X + (j * dx), topLeft.Y + (i * dy), topLeft.Z);
                    Points[i, j] = point;
                }
        }
        private void SetDirections()
        {
            switch (Name)
            {
                case "glowna":
                    IntersectionDirection = new List<int>() { 0, 0, 1, 2, 2 };
                    break;
                case "czub":
                    IntersectionDirection = new List<int>() { 0, 0, 1, 1 };
                    break;
                case "przednia":
                    IntersectionDirection = new List<int>() { 0, 0, 1 };
                    break;
                case "tylnia":
                    IntersectionDirection = new List<int>() { 0, 0, 1 };
                    break;
                case "wiertlo":
                    IntersectionDirection = new List<int>() { 0, 0, 1 };
                    break;
            }
        }
        protected void SetVertices(Point3D[,] points, int verticalPoints, int horizontalPoints)
        {
            if (IsCylinder)
            {
                if (points == null)
                {
                    if (Continuity == 2) Points = new Point3D[verticalPoints, horizontalPoints - 3];
                    else Points = new Point3D[verticalPoints, horizontalPoints - 1];
                    SetCylinderVertices();
                }
                else
                {
                    Points = points;
                }
            }
            else
            {
                if (points == null)
                {
                    Points = new Point3D[verticalPoints, horizontalPoints];
                    SetPlaneVertices();
                }
                else
                {
                    Points = points;
                }
            }
        }
        protected abstract Point3D[,] GetPatchMatrix(int i, int j);
        #endregion Protected Methods
        #region Public Methods

        public abstract Point3D CalculatePointOnSurface(int i, int j, double u, double v, bool uDerivative = false, bool vDerivative = false);
        #endregion Public Methods

        public void SetSurfacePointList()
        {
            MappingPoint = new List<int[]>();
            SurfacePoints = new List<Point3D>();
            int count = 0;
            for (int a = 0; a < Points.GetLength(0); a++)
                for (int b = 0; b < Points.GetLength(1); b++)
                    if (!SurfacePoints.Contains(Points[a, b]))
                    {
                        SurfacePoints.Add(Points[a, b]);
                        MappingPoint.Add(new int[] { a, b, count++ });
                    }
                    else
                        MappingPoint.Add(new int[] { a, b, SurfacePoints.IndexOf(Points[a, b]) });
        }

        public void UpdateVertices()
        {
            int[] tmp;
            for (int i = 0; i < MappingPoint.Count; i++)
            {
                tmp = MappingPoint[i];
                Points[tmp[0], tmp[1]] = SurfacePoints[tmp[2]];
            }
        }
    }
}

