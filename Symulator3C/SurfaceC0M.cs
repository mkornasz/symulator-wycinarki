﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Media.Media3D;

namespace Symulator3C
{
    public class SurfaceC0 : SurfaceM
    {
        #region Public Properties
        #endregion Public Properties
        #region Constructors
        public SurfaceC0(double x, double y, double z, string name, bool isCylinder, double width, double height, int verticalPatches, int horizontalPatches, Point3D[,] points = null)
            : base(x, y, z, name, isCylinder, width, height, verticalPatches, horizontalPatches)
        {
            SetVertices(points, 3 * VerticalPatches + 1, 3 * HorizontalPatches + 1);// manager.VerticalPatches * SceneManager.BezierSegmentPoints + 1, manager.HorizontalPatches * SceneManager.BezierSegmentPoints + 1);
            Continuity = 0;

        }
        #endregion Constructors
        #region Private Methods


        public Point3D CalculatePatchPoint(Matrix3D matX, Matrix3D matY, Matrix3D matZ, Vector4 pointU, Vector4 pointV)
        {

            var x = pointU * matX * pointV;
            var y = pointU * matY * pointV;
            var z = pointU * matZ * pointV;
            //   var point = new Vector4(x, y, z, 1);
            return new Point3D(x, y, z);
        }
        #endregion Private Methods
        #region Protected Methods

        protected override Point3D[,] GetPatchMatrix(int i, int j)
        {
            const int bezierSegmentPoints = 3;
            return new[,]{{Points[i * bezierSegmentPoints + 0, j * bezierSegmentPoints], Points[i * bezierSegmentPoints + 0, j * bezierSegmentPoints + 1], Points[i * bezierSegmentPoints + 0, j * bezierSegmentPoints + 2], Points[i * bezierSegmentPoints + 0, (j * bezierSegmentPoints + 3) % Points.GetLength(1)]}
                        , {Points[i * bezierSegmentPoints + 1, j * bezierSegmentPoints], Points[i * bezierSegmentPoints + 1, j * bezierSegmentPoints + 1], Points[i * bezierSegmentPoints + 1, j * bezierSegmentPoints + 2], Points[i * bezierSegmentPoints + 1, (j * bezierSegmentPoints + 3) % Points.GetLength(1)]}
                        , {Points[i * bezierSegmentPoints + 2, j * bezierSegmentPoints], Points[i * bezierSegmentPoints + 2, j * bezierSegmentPoints + 1], Points[i * bezierSegmentPoints + 2, j * bezierSegmentPoints + 2], Points[i * bezierSegmentPoints + 2, (j * bezierSegmentPoints + 3) % Points.GetLength(1)]}
                        , {Points[i * bezierSegmentPoints + 3, j * bezierSegmentPoints], Points[i * bezierSegmentPoints + 3, j * bezierSegmentPoints + 1], Points[i * bezierSegmentPoints + 3, j * bezierSegmentPoints + 2], Points[i * bezierSegmentPoints + 3, (j * bezierSegmentPoints + 3) % Points.GetLength(1)]}};
        }
        #endregion Protected Methods
        #region Public Methods

        public override Point3D CalculatePointOnSurface(int i, int j, double u, double v, bool uDerivative = false, bool vDerivative = false)
        {
            var pointX = uDerivative ? GetBezierDerivativePoint(u) : GetBezierPoint(u);
            var pointY = vDerivative ? GetBezierDerivativePoint(v) : GetBezierPoint(v);

            var matrix = GetPatchMatrix(i, j);

            var matX = new Matrix3D(matrix[0, 0].X, matrix[0, 1].X, matrix[0, 2].X, matrix[0, 3].X
                                  , matrix[1, 0].X, matrix[1, 1].X, matrix[1, 2].X, matrix[1, 3].X
                                  , matrix[2, 0].X, matrix[2, 1].X, matrix[2, 2].X, matrix[2, 3].X
                                  , matrix[3, 0].X, matrix[3, 1].X, matrix[3, 2].X, matrix[3, 3].X);
            var matY = new Matrix3D(matrix[0, 0].Y, matrix[0, 1].Y, matrix[0, 2].Y, matrix[0, 3].Y
                                  , matrix[1, 0].Y, matrix[1, 1].Y, matrix[1, 2].Y, matrix[1, 3].Y
                                  , matrix[2, 0].Y, matrix[2, 1].Y, matrix[2, 2].Y, matrix[2, 3].Y
                                  , matrix[3, 0].Y, matrix[3, 1].Y, matrix[3, 2].Y, matrix[3, 3].Y);
            var matZ = new Matrix3D(matrix[0, 0].Z, matrix[0, 1].Z, matrix[0, 2].Z, matrix[0, 3].Z
                                  , matrix[1, 0].Z, matrix[1, 1].Z, matrix[1, 2].Z, matrix[1, 3].Z
                                  , matrix[2, 0].Z, matrix[2, 1].Z, matrix[2, 2].Z, matrix[2, 3].Z
                                  , matrix[3, 0].Z, matrix[3, 1].Z, matrix[3, 2].Z, matrix[3, 3].Z);

            return CalculatePatchPoint(matX, matY, matZ, pointX, pointY);
        }
        #endregion Public Methods

        private Vector4 GetBezierPoint(double v)
        {
            return new Vector4(Math.Pow((1.0 - v), 3), 3 * v * Math.Pow((1.0 - v), 2), 3 * v * v * (1.0 - v), Math.Pow(v, 3));
        }
        /// <summary>
        /// Gets the bezier derivative point.
        /// </summary>
        /// <param name="v">The v.</param>
        /// <returns>Bezier point: [B_3_0', B_3_1', B_3_2', B_3_3']</returns>
        private Vector4 GetBezierDerivativePoint(double v)
        {
            return new Vector4(-3.0 * Math.Pow((1.0 - v), 2), (9.0 * v * v) - (12.0 * v) + 3.0, (6.0 * v) - (9.0 * v * v), 3 * v * v);
        }

    }
}

